package com.rifqidmw.tshirtapp.data.local.model

class KesatuanModel {
    lateinit var status: String
    lateinit var msg: String
    lateinit var data: ArrayList<Data>

    class Data{
        lateinit var _id: String
        lateinit var name: String
        lateinit var createdBy: String
        lateinit var createdAt: String
    }
}