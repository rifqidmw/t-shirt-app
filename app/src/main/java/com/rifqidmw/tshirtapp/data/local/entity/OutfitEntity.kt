package com.rifqidmw.tshirtapp.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "outfit", indices = [Index(value = ["_id"], unique = true)])
data class OutfitEntity(
    @ColumnInfo(name = "_id") var _id: String ,
    @ColumnInfo(name = "pakaian") var pakaian: String,
    @ColumnInfo(name = "value") var value: String,
    @ColumnInfo(name = "kategori") var kategori: String,
    @ColumnInfo(name = "sub_kategori") var subKategori: String,
    @ColumnInfo(name = "userId") var userId: String,
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Int = 0
)