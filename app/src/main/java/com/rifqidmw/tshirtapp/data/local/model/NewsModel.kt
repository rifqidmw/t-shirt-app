package com.rifqidmw.tshirtapp.data.local.model

class NewsModel {
    lateinit var status: String
    lateinit var msg: String
    lateinit var data: ArrayList<Data>

    class Data{
        lateinit var _id: String
        lateinit var title: String
        lateinit var content: String
        lateinit var createdAt: String
    }
}