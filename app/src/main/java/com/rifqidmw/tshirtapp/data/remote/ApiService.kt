package com.rifqidmw.tshirtapp.data.remote

import com.google.gson.JsonObject
import com.rifqidmw.tshirtapp.data.local.model.*
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.http.*

interface ApiService {

    @POST("login")
    fun doLogin(
        @HeaderMap headers: Map<String, String>,
        @Body data: JsonObject
    ) : Single<LoginModel>

    @POST("register")
    fun doRegister(
        @Body data: JsonObject
    ) : Single<RegisterModel>

    @PUT("user/{userId}")
    fun doUpdateProfile(
        @HeaderMap headers: Map<String, String>,
        @Body data: JsonObject,
        @Path("userId") userId: String
    ) : Single<UserDetailModel>

    @PUT("ukuran")
    fun doAddSize(
        @HeaderMap headers: Map<String, String>,
        @Body data: JsonObject
    ) : Single<SizeModel>

    @GET("user/{userId}")
    fun getUserDetail(
        @HeaderMap headers: Map<String, String>,
        @Path("userId") userId: String
    ) : Single<UserDetailModel>

    @GET("berita")
    fun getNews(
        @HeaderMap headers: Map<String, String>
    ) : Single<NewsModel>

    @GET("ukuran")
    fun getSize(
        @HeaderMap headers: Map<String, String>
    ) : Single<SizeModel>

    @GET("kesatuan")
    fun getKesatuan() : Flowable<KesatuanModel>

    @GET("perlengkapan")
    fun getPerlengkapan() : Single<PerlengkapanModel>

    @GET("kategori")
    fun getKategori() : Single<KategoriModel>
    /*@GET("boxnovel?")
    fun getListNovel(
        @Query("page") page: String
    ) : Observable<ListNovelModel>

    @GET("boxnovel/novel/filter")
    fun getFilteredNovel(
        @Query("page") page: String,
        @Query("type") type: String
    ) : Observable<ListNovelModel>

    @GET("boxnovel/novel/country")
    fun getNovelByCountry(
        @Query("page") page: String,
        @Query("country") country: String,
        @Query("type") type: String
    ) : Observable<ListNovelModel>

    @GET
    fun getDetailNovel(
        @Url url: String
    ) : Observable<DetailNovelModel>

    @GET
    fun getChapterNovel(
        @Url url: String
    ) : Observable<ChapterNovelModel>*/
}