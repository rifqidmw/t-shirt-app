package com.rifqidmw.tshirtapp.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rifqidmw.tshirtapp.data.local.entity.KategoriEntity
import com.rifqidmw.tshirtapp.data.local.entity.KesatuanEntity
import com.rifqidmw.tshirtapp.data.local.entity.OutfitEntity
import com.rifqidmw.tshirtapp.data.local.entity.PerlengkapanEntity
import com.rifqidmw.tshirtapp.data.local.model.KesatuanModel

@Dao
interface MainDao {
    //GET
    //Kesatuan
    @Query("SELECT * from kesatuan ORDER BY name")
    fun getKesatuan(): List<KesatuanEntity>

    @Query("SELECT * from kesatuan WHERE _id=:id LIMIT 1")
    fun getKesatuanById(id: String): KesatuanEntity

    //Perlengkapan
    @Query("SELECT * from perlengkapan ORDER BY name")
    fun getPerlengkapan(): List<PerlengkapanEntity>

    @Query("SELECT * from perlengkapan WHERE _id=:id LIMIT 1")
    fun getPerlengkapanById(id: String): PerlengkapanEntity

    @Query("SELECT * from perlengkapan t1 WHERE category=:category AND NOT EXISTS(SELECT * FROM outfit t2 WHERE t1._id = t2._id) ORDER BY name")
    fun getPerlengkapanByCategory(category: String): List<PerlengkapanEntity>

    @Query("SELECT * from perlengkapan t1 WHERE category=:category AND sub_category=:subCategory AND NOT EXISTS(SELECT * FROM outfit t2 WHERE t1._id = t2._id) ORDER BY name")
    fun getPerlengkapanByCategoryAndSub(category: String, subCategory: String): List<PerlengkapanEntity>

    @Query("SELECT * from perlengkapan t1 WHERE NOT EXISTS(SELECT * FROM outfit t2 WHERE t1._id = t2._id) ORDER BY name")
    fun getPerlengkapanByExist(): List<PerlengkapanEntity>

    //Kategori
    @Query("SELECT * from kategori WHERE parent='' ORDER BY name")
    fun getKategori(): List<KategoriEntity>

    @Query("SELECT * from kategori WHERE _id=:id AND parent='' LIMIT 1")
    fun getKategoriById(id: String): KategoriEntity

    //Sub Kategori
    @Query("SELECT * from kategori WHERE parent=:id ORDER BY name")
    fun getSubKategori(id: String): List<KategoriEntity>

    @Query("SELECT * from kategori WHERE parent=:id LIMIT 1")
    fun getSubKategoriById(id: String): KategoriEntity

    //Outfit
    @Query("SELECT * from outfit WHERE userId=:userId ORDER BY pakaian")
    fun getOutfit(userId: String): List<OutfitEntity>

    @Query("SELECT * from outfit WHERE userId=:userId AND _id=:id ORDER BY pakaian")
    fun getOutfitById(userId: String, id: String): List<OutfitEntity>

    //ADD
    //Kesatuan
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addKesatuan(kesatuanModel: KesatuanEntity)
    //Perlengkapan
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addPerlengkapan(perlengkapanModel: PerlengkapanEntity)
    //Kategori
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addKategori(kategoriModel: KategoriEntity)
    //Outfit
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addOutfit(outfitModel: OutfitEntity)

    //UPDATE
    @Query("UPDATE outfit SET value = :value WHERE _id=:id AND userId = :user_id")
    fun updateOutfitValue(user_id: String, id: String, value: String)

    //DELETE
    @Query("DELETE FROM outfit WHERE userId=:userId")
    fun deleteOutfitByUser(userId:String)
}