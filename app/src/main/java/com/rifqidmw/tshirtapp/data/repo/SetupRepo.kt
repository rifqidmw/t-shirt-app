package com.rifqidmw.tshirtapp.data.repo

import com.google.gson.JsonObject
import com.rifqidmw.tshirtapp.data.local.model.*
import com.rifqidmw.tshirtapp.data.remote.ApiService
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

class SetupRepo (private val service: ApiService) {
    fun getKesatuan() : Flowable<KesatuanModel>{
        return service.getKesatuan()
    }

    fun getPerlengkapan() : Single<PerlengkapanModel>{
        return service.getPerlengkapan()
    }

    fun getKategori() : Single<KategoriModel>{
        return service.getKategori()
    }
}