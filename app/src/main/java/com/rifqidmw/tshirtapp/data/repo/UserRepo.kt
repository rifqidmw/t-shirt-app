package com.rifqidmw.tshirtapp.data.repo

import com.google.gson.JsonObject
import com.rifqidmw.tshirtapp.data.local.model.KesatuanModel
import com.rifqidmw.tshirtapp.data.local.model.LoginModel
import com.rifqidmw.tshirtapp.data.local.model.RegisterModel
import com.rifqidmw.tshirtapp.data.local.model.UserDetailModel
import com.rifqidmw.tshirtapp.data.remote.ApiService
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

class UserRepo (private val service: ApiService) {
    fun doLogin(header: Map<String, String>, data: JsonObject) : Single<LoginModel>{
        return service.doLogin(header, data)
    }

    fun getUserDetail(header: Map<String, String>, userId: String) : Single<UserDetailModel>{
        return service.getUserDetail(header, userId)
    }

    fun doRegister(data: JsonObject) : Single<RegisterModel>{
        return service.doRegister(data)
    }

    fun doUpdateProfile(header: Map<String, String>, data: JsonObject, userId: String) : Single<UserDetailModel>{
        return service.doUpdateProfile(header, data, userId)
    }

    fun getKesatuan() : Flowable<KesatuanModel> {
        return service.getKesatuan()
    }
}