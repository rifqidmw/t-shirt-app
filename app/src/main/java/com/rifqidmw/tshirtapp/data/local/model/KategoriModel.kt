package com.rifqidmw.tshirtapp.data.local.model

class KategoriModel {
    lateinit var status: String
    lateinit var msg: String
    lateinit var data: ArrayList<Data>

    class Data{
        lateinit var _id: String
        lateinit var name: String
        lateinit var parent: String
    }
}