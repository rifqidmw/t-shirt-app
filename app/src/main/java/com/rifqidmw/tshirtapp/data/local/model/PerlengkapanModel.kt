package com.rifqidmw.tshirtapp.data.local.model

class PerlengkapanModel {
    lateinit var status: String
    lateinit var msg: String
    lateinit var data: ArrayList<Data>

    class Data{
        lateinit var _id: String
        lateinit var name: String
        lateinit var ukuran: ArrayList<String>
        lateinit var categori: String
        lateinit var subcategori: String
        lateinit var imageurl: String
        lateinit var file: File

        class File{
            lateinit var name: String
            lateinit var id: String
        }
    }
}