package com.rifqidmw.tshirtapp.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "perlengkapan", indices = [Index(value = ["_id"], unique = true)])
data class PerlengkapanEntity(
    @ColumnInfo(name = "_id") var _id: String ,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "imageurl") var imgurl: String,
    @ColumnInfo(name = "ukuran") var ukuran: List<String>,
    @ColumnInfo(name = "category") var category: String,
    @ColumnInfo(name = "sub_category") var subCategory: String,
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Int = 0
)