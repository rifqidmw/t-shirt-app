package com.rifqidmw.tshirtapp.data.repo

import com.google.gson.JsonObject
import com.rifqidmw.tshirtapp.data.local.model.LoginModel
import com.rifqidmw.tshirtapp.data.local.model.SizeModel
import com.rifqidmw.tshirtapp.data.local.model.UserDetailModel
import com.rifqidmw.tshirtapp.data.remote.ApiService
import io.reactivex.Observable
import io.reactivex.Single

class InputRepo (private val service: ApiService) {
    fun doAddSize(header: Map<String, String>, data: JsonObject) : Single<SizeModel>{
        return service.doAddSize(header, data)
    }
}