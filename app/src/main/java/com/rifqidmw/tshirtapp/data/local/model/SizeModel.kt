package com.rifqidmw.tshirtapp.data.local.model

class SizeModel {
    lateinit var status: String
    lateinit var msg: String
    lateinit var data: List<Data>

    class Data{
        lateinit var id: String
        lateinit var pakaian: String
        lateinit var value: String
    }
}