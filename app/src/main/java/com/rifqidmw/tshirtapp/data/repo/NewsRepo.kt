package com.rifqidmw.tshirtapp.data.repo

import com.google.gson.JsonObject
import com.rifqidmw.tshirtapp.data.local.model.LoginModel
import com.rifqidmw.tshirtapp.data.local.model.NewsModel
import com.rifqidmw.tshirtapp.data.local.model.UserDetailModel
import com.rifqidmw.tshirtapp.data.remote.ApiService
import io.reactivex.Observable
import io.reactivex.Single

class NewsRepo (private val service: ApiService) {
    fun getNews(header: Map<String, String>) : Single<NewsModel>{
        return service.getNews(header)
    }
}