package com.rifqidmw.tshirtapp.data.local.model

class UserDetailModel {
    lateinit var status: String
    lateinit var msg: String
    lateinit var data: Data

    class Data{
        lateinit var _id: String
        lateinit var username: String
        lateinit var emails: ArrayList<Email>
        lateinit var profile: Profile

        class Email{
            lateinit var address: String
            lateinit var verified: String
        }

        class Profile{
            lateinit var nrp: String
            lateinit var phone: String
            lateinit var jenis_kelamin: String
            lateinit var kesatuan: String
        }
    }
}