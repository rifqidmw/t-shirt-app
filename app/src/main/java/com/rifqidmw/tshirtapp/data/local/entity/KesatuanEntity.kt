package com.rifqidmw.tshirtapp.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "kesatuan", indices = [Index(value = ["_id"], unique = true)])
data class KesatuanEntity(
    @ColumnInfo(name = "_id") var _id: String ,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "createdBy") var createdBy: String,
    @ColumnInfo(name = "createdAt") var createdAt: String,
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Int = 0
)