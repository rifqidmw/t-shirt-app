package com.rifqidmw.tshirtapp.data.local.model

class LoginModel {
    lateinit var status: String
    lateinit var data: Data

    class Data {
        lateinit var authToken: String
        lateinit var userId: String
    }
}