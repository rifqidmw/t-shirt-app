package com.rifqidmw.tshirtapp

import android.content.Context
import com.rifqidmw.tshirtapp.BuildConfig.BASE_URL_API
import com.google.gson.Gson
import com.rifqidmw.tshirtapp.data.local.MainDatabase
import com.rifqidmw.tshirtapp.data.local.Session
import com.rifqidmw.tshirtapp.data.remote.ApiService
import com.rifqidmw.tshirtapp.data.repo.*
import com.rifqidmw.tshirtapp.ui.editProfile.EditProfileViewModel
import com.rifqidmw.tshirtapp.ui.input.InputViewModel
import com.rifqidmw.tshirtapp.ui.login.LoginViewModel
import com.rifqidmw.tshirtapp.ui.main.fragment.Outfit.OutfitViewModel
import com.rifqidmw.tshirtapp.ui.main.fragment.home.HomeViewModel
import com.rifqidmw.tshirtapp.ui.main.fragment.news.NewsViewModel
import com.rifqidmw.tshirtapp.ui.register.RegisterViewModel
import com.rifqidmw.tshirtapp.ui.splash.SplashViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val appModule = module {
    single { createOkHttpClient() }
    single { createRetrofit(get()) }
    single { createApiService(get()) }

    //repo
    factory { SetupRepo(get()) }
    factory { UserRepo(get()) }
    factory { NewsRepo(get()) }
    factory { InputRepo(get()) }
    factory { OutfitRepo(get()) }

    //viewmodel
    viewModel { SplashViewModel(get()) }
    viewModel { LoginViewModel(get(), get()) }
    viewModel { RegisterViewModel(get()) }
    viewModel { NewsViewModel(get()) }
    viewModel { InputViewModel(get()) }
    viewModel { OutfitViewModel(get()) }
    viewModel { EditProfileViewModel(get()) }
    viewModel { HomeViewModel(get(), get()) }
    /*viewModel { MainViewModel(get()) }
    viewModel { DetailNovelViewModel(get()) }
    viewModel { FilteredNovelViewModel(get()) }
    viewModel { ChapterNovelViewModel(get()) }*/
}

val dataModule = module {
    single { createSharedPref(androidContext()) }
    single { createRoomDatabase(androidContext()) }
}

fun createOkHttpClient(): OkHttpClient{

    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

    return OkHttpClient.Builder()
        .connectTimeout(60L, TimeUnit.SECONDS)
        .readTimeout(60L, TimeUnit.SECONDS)
        .writeTimeout(60L, TimeUnit.SECONDS)
        .addInterceptor(httpLoggingInterceptor)
        .build()

}

fun createRetrofit(okHttpClient: OkHttpClient): Retrofit{
    return Retrofit.Builder()
        .baseUrl(BASE_URL_API)
        .addConverterFactory(GsonConverterFactory.create(Gson()))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
        .client(okHttpClient)
        .build()
}

fun createApiService(retrofit: Retrofit) : ApiService = retrofit.create(
    ApiService::class.java)

fun createSharedPref(context: Context) : Session =
    Session(context)

fun createRoomDatabase(context: Context) : MainDatabase = MainDatabase.getInstance(context)!!