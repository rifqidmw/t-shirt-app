package com.rifqidmw.novel_kun.utls

sealed class ViewModelState{
    data class Loading(var isLoading: Boolean) : ViewModelState()
    data class Failed(var error: String?="",var code:String?=null) : ViewModelState()
}