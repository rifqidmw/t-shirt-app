package com.rifqidmw.tshirtapp.utils

object Constant {
    const val IS_FIRST_TIME = "is_first_time"

    //User
    const val AUTH_TOKEN = "authToken"
    const val USER_ID = "_id"
    const val USER_PASSWORD = "user_password"
    const val USERNAME = "username"
    const val EMAIL = "address"
    const val EMAIL_VERIFIED = "verified"
    const val NRP = "nrp"
    const val PHONE = "phone"
    const val JK = "jenis_kelamin"
    const val KESATUAN = "kesatuan"
}