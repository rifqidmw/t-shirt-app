package com.rifqidmw.tshirtapp.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.VibrationEffect
import android.os.Vibrator
import android.provider.MediaStore
import android.provider.Settings
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.rifqidmw.tshirtapp.BuildConfig
import com.rifqidmw.tshirtapp.R
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.regex.Pattern


fun toJsonElement(any: Any): JsonElement = Gson().toJsonTree(any)


fun logI(tag: String, msg: String) {
    if (BuildConfig.DEBUG) Log.i(tag, msg)
}

fun logW(tag: String, msg: String) {
    if (BuildConfig.DEBUG) Log.w(tag, msg)
}

fun logE(tag: String, msg: String) {
    if (BuildConfig.DEBUG) Log.e(tag, msg)
}

fun logD(tag: String, msg: String) {
    if (BuildConfig.DEBUG) Log.d(tag, msg)
}

fun logV(tag: String,msg: String){
    if (BuildConfig.DEBUG) Log.v(tag,msg)
}

fun Context.toast(message :String){
    Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
}

fun Context.toastLong(message :String){
    Toast.makeText(this,message,Toast.LENGTH_LONG).show()
}

fun regex(): Regex{
    val regex = Regex ("[^A-Za-z0-9 ]")

    return regex
}

fun getDateFormat(): SimpleDateFormat{
    val dateFormat = SimpleDateFormat("MM/dd/yy, hh:mm:ss a")

    return dateFormat
}

fun getTimeNow(format:String ="MM/dd/yy, hh:mm:ss a"):String{
    val dateFormat = SimpleDateFormat(format)
    return dateFormat.format(Date())
}

fun getTimestamp(): String{
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        return DateTimeFormatter.ISO_INSTANT.format(Instant.now()).toString()
    } else {
        return System.currentTimeMillis().toString()
    }
}

fun deleteFile(target: String) {
    val path = File(target)
    path.delete()
    if (path.exists() && path.isDirectory) {
        for (child in path.listFiles()!!) {
            child.delete()
        }
    }
}

fun hideKeyboard(activity: Activity){
    val imm: InputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view = activity.currentFocus

    if (view == null){
        view = View(activity)
    }

    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Long.timeMillisToDateTime(format: String?=null):String{
    var format = format
    if (format == null){
        format = "MM/dd/yy, hh:mm:ss aa"
    }
    val sdf = SimpleDateFormat(format)
    return sdf.format(Date(this))
}

fun Long.timeMillisToDate(format: String?=null):String{
    var format = format
    if (format == null){
        format = "MM/dd/yy"
    }
    val sdf = SimpleDateFormat(format)
    return sdf.format(Date(this))
}

fun Long.timeMillisToTime(format: String?=null):String{
    var format = format
    if (format == null){
        format = "hh:mm a"
    }
    val sdf = SimpleDateFormat(format)
    return sdf.format(Date(this))
}

fun Context.showSettingsDialog(doPositiveButton: (() -> Unit?)? =null,doNegativeButton: (() -> Unit?)?=null){
    val builder = AlertDialog.Builder(this)
    builder.setTitle(getString(R.string.dialog_permission_title))
    builder.setMessage(getString(R.string.dialog_permission_message))
    builder.setPositiveButton(getString(R.string.go_to_settings)) { dialog, which ->
        dialog.cancel()
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivity(intent)
        doPositiveButton?.invoke()
    }
    builder.setNegativeButton(getString(android.R.string.cancel)) { dialog, which ->
        dialog.cancel()
        doNegativeButton?.invoke()
    }
    builder.show()
}

fun Activity.setupPermissionsCameraView(doGranted: () -> Unit,doOnNoGrandted:() -> Unit ) {

    Dexter.withActivity(this)
        .withPermissions(
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE)
        .withListener(object : MultiplePermissionsListener {
            override fun onPermissionsChecked(report: MultiplePermissionsReport) {

                if (report.areAllPermissionsGranted()) {
                    doGranted()
                }



                if (report.isAnyPermissionPermanentlyDenied) {
                    doOnNoGrandted()
                }

            }

            override fun onPermissionRationaleShouldBeShown(
                permissions: MutableList<PermissionRequest>?,
                token: PermissionToken?
            ) {
                token?.continuePermissionRequest()
            }
        }
        ).check()

}





