package com.rifqidmw.tshirtapp.ui.main.fragment.Outfit.adapter

import androidx.recyclerview.widget.DiffUtil
import com.rifqidmw.tshirtapp.data.local.entity.OutfitEntity

class OutfitAdapterDiffUtilCallback(private val oldData: MutableList<OutfitEntity>,
                                    private val newData: MutableList<OutfitEntity>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int,
                                 newItemPosition: Int): Boolean {
        return oldData[oldItemPosition]._id == newData[newItemPosition]._id
    }

    override fun getOldListSize(): Int = oldData.size

    override fun getNewListSize(): Int = newData.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldData[oldItemPosition] == newData[newItemPosition]
    }
}