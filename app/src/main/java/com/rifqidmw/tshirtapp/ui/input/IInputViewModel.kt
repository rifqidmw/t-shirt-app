package com.rifqidmw.tshirtapp.ui.input

import com.rifqidmw.tshirtapp.data.local.model.SizeModel

interface IInputViewModel {
    fun successAddSize(sizeModel: SizeModel)
    fun errorLoad(t: Throwable)
}