package com.rifqidmw.tshirtapp.ui.input.adapter

import android.view.View
import com.rifqidmw.tshirtapp.data.local.entity.KategoriEntity
import com.rifqidmw.tshirtapp.data.local.model.KesatuanModel

interface CategoryAdapterClickListener {

    fun onItemCategoryClicked(data: KategoriEntity, type: String)

}