package com.rifqidmw.tshirtapp.ui.editProfile

import com.rifqidmw.tshirtapp.data.local.model.LoginModel
import com.rifqidmw.tshirtapp.data.local.model.RegisterModel
import com.rifqidmw.tshirtapp.data.local.model.UserDetailModel
import com.rifqidmw.tshirtapp.ui.login.LoginState

sealed class EditProfileState {
    data class OnSuccessUpdateProfile(val userDetailModel: UserDetailModel): EditProfileState()
    data class OnSuccessLogin(val loginModel: LoginModel): EditProfileState()
    data class OnErrorState(val t: Throwable): EditProfileState()
}