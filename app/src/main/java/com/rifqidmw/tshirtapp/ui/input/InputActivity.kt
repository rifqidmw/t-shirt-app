package com.rifqidmw.tshirtapp.ui.input

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.google.android.material.shape.CornerFamily
import com.google.gson.JsonObject
import com.rifqidmw.tshirtapp.R
import com.rifqidmw.tshirtapp.data.local.MainDatabase
import com.rifqidmw.tshirtapp.data.local.Session
import com.rifqidmw.tshirtapp.data.local.entity.KategoriEntity
import com.rifqidmw.tshirtapp.data.local.entity.OutfitEntity
import com.rifqidmw.tshirtapp.data.local.entity.PerlengkapanEntity
import com.rifqidmw.tshirtapp.data.local.model.SizeModel
import com.rifqidmw.tshirtapp.ui.input.adapter.*
import com.rifqidmw.tshirtapp.utils.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_input.*
import kotlinx.android.synthetic.main.dialog_category.*
import org.json.JSONException
import org.koin.android.ext.android.inject
import java.io.IOException
import java.util.HashMap

class InputActivity : AppCompatActivity(), Observer<InputState>, OutfitAdapterClickListener,
    CategoryAdapterClickListener, RadioSizeAdapterClickListener {

    private val TAG = InputActivity::class.java.simpleName

    private val session: Session by inject()
    private val mViewModel: InputViewModel by inject()
    private val compositeDisposable = CompositeDisposable()
    private val mainDatabase: MainDatabase by inject()
    private val REQUEST_IMAGE = 100

    private lateinit var radioSizeAdapter: RadioSizeAdapter
    private lateinit var outfitAdapter: OutfitAdapter
    private lateinit var categoryAdapter: CategoryAdapter
    private lateinit var alertDialogOutfit: AlertDialog
    private lateinit var alertDialogCategory: AlertDialog

    private var dataSize: ArrayList<String> = ArrayList()
    private var dataOutfit: ArrayList<PerlengkapanEntity> = ArrayList()
    private var dataCategory: ArrayList<KategoriEntity> = ArrayList()
    private var dataSubCategory: ArrayList<KategoriEntity> = ArrayList()

    private var valueOutfit: PerlengkapanEntity? = null
    private var valueCategory: String = ""
    private var valueSubCategory: String = ""
    private var valueSize: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_input)

        mViewModel.state.observe(this, this@InputActivity)

        setupView()

        getCategory()
        setupAdapterSize()
    }

    private fun setupView(){
        btn_photo.setOnClickListener {
            onImageClick()
        }

        btn_outfit.setOnClickListener {
            if (valueCategory.isNotEmpty()){
                if (dataSubCategory.isNotEmpty()){
                    if (valueSubCategory.isNotEmpty()){
                        showDialogOutfit()
                    }else {
                        toast("Please choose sub category first")
                    }
                } else {
                    showDialogOutfit()
                }
            } else {
                toast("Please choose category first")
            }
        }

        btn_category.setOnClickListener {
            showDialogCategory("category")
        }

        btn_sub_category.setOnClickListener {
            if (valueCategory.isNotEmpty()){
                if (dataSubCategory.isNotEmpty()){
                    showDialogCategory("sub")
                } else {
                    toast("No sub category")
                }
            } else {
                toast("Please choose category first")
            }
        }

        btn_save.setOnClickListener {
            doInputSize()
        }

        toolbar.setNavigationOnClickListener {
            onBackPressed()
            this@InputActivity.finish()
        }
    }

    private fun getPerlengkapanExist(){
        if (valueCategory.isEmpty()){
            logD(TAG, "get perlengkapan all")
            compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getPerlengkapanByExist()}
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dataOutfit.clear()
                    dataOutfit.addAll(it)
                },{}))

            compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getOutfit(session[Constant.USER_ID]!!)}
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    logD(TAG, it.joinToString())
                },{}))
        } else if (valueCategory.isNotEmpty() && valueSubCategory.isEmpty()){
            getPerlengkapanByCategory()
        } else if (valueCategory.isNotEmpty() && valueSubCategory.isNotEmpty()){
            getPerlengkapanByCategoryAndSub()
        }
    }

    private fun getPerlengkapanByCategory(){
        logD(TAG, "get perlengkapan by category")
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getPerlengkapanByCategory(valueCategory)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                dataOutfit.clear()
                dataOutfit.addAll(it)
            },{}))
    }

    private fun getPerlengkapanByCategoryRandom(){
        logD(TAG, "get perlengkapan by category random")
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getPerlengkapanByCategory(valueCategory)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                ly_sub_category.visibility = View.VISIBLE

                val dataRandom = it.random()
                tv_outfit.text = dataRandom.name
                tv_sub_category.text = "No sub category"

                valueOutfit = dataRandom

                dataSize.clear()
                dataSize.addAll(dataRandom.ukuran)
                radioSizeAdapter.resetAdapter()
                radioSizeAdapter.notifyDataSetChanged()

                radioSizeAdapter.updateData(dataRandom.ukuran.random())
            },{}))
    }

    private fun getPerlengkapanByCategoryAndSub(){
        logD(TAG, "get perlengkapan by category & sub category random")
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getPerlengkapanByCategoryAndSub(valueCategory, valueSubCategory)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                dataOutfit.clear()
                dataOutfit.addAll(it)
            },{}))
    }

    private fun getPerlengkapanByCategoryAndSubRandom(){
        logD(TAG, "get perlengkapan by category & sub category")
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getPerlengkapanByCategoryAndSub(valueCategory, valueSubCategory)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                ly_sub_category.visibility = View.VISIBLE

                val dataRandom = it.random()
                tv_outfit.text = dataRandom.name

                valueOutfit = dataRandom


                dataSize.clear()
                dataSize.addAll(dataRandom.ukuran)
                radioSizeAdapter.resetAdapter()
                radioSizeAdapter.notifyDataSetChanged()

                radioSizeAdapter.updateData(dataRandom.ukuran.random())
            },{}))
    }

    private fun getPerlengkapanById(data: SizeModel.Data){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getPerlengkapanById(data.id)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                addOutfit(OutfitEntity(it._id, it.name, data.value, it.category, it.subCategory, session[Constant.USER_ID]!!))
            },{}))
    }

    private fun getCategory(){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getKategori()}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                dataCategory.clear()
                dataCategory.addAll(it)
                categoryAdapter.notifyDataSetChanged()
            },{}))
    }

    private fun getCategoryRandom(){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getKategori()}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                dataCategory.clear()
                dataCategory.addAll(it)

                if (valueOutfit == null){
                    val categoryRandom = it.random()
                    tv_category.text = categoryRandom.name
                    valueCategory = categoryRandom.name

                    getSubCategoryRandom(categoryRandom._id)

                } else {
                    dataSize.clear()
                    dataSize.addAll(valueOutfit!!.ukuran)
                    radioSizeAdapter.resetAdapter()
                    radioSizeAdapter.notifyDataSetChanged()

                    radioSizeAdapter.updateData(valueOutfit!!.ukuran.random())
                }
            },{}))
    }

    private fun getSubCategory(id: String){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getSubKategori(id)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                dataSubCategory.clear()
                dataSubCategory.addAll(it)
                categoryAdapter.notifyDataSetChanged()

                tv_sub_category.text = if (it.isNotEmpty()){
                    "Choose sub category"
                } else {
                    "No sub category"
                }
            },{}))
    }

    private fun getSubCategoryRandom(id: String){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getSubKategori(id)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                dataSubCategory.clear()
                dataSubCategory.addAll(it)

                val subCategoryRandom = if (it.isNotEmpty()){
                    it.random().name
                } else {
                    ""
                }
                valueSubCategory = subCategoryRandom

                tv_sub_category.text = if (it.isNotEmpty()){
                    subCategoryRandom
                } else {
                    "No sub category"
                }

                if (subCategoryRandom.isEmpty()){
                    getPerlengkapanByCategoryRandom()
                } else {
                    getPerlengkapanByCategoryAndSubRandom()
                }
            },{}))
    }

    private fun addOutfit(outfit: OutfitEntity){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().addOutfit(outfit)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Glide.with(this@InputActivity).clear(img_product)
                layout_no_image.visibility = View.VISIBLE
                img_product.visibility = View.GONE
                ly_sub_category.visibility = View.GONE

                val intent = Intent(this@InputActivity, InputSuccessActivity::class.java)
                startActivity(intent)
            },{}))
    }

    private fun getOutfitById(id: String){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getOutfitById(session[Constant.USER_ID]!!,id)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

            },{}))
    }

    private fun doInputSize(){
        if (valueOutfit != null && valueSize != 0){
            btn_save.startAnimation()

            val headers = HashMap<String, String>()
            headers["Content-Type"] = "application/json"
            headers["X-Auth-Token"] = session[Constant.AUTH_TOKEN].toString()
            headers["X-User-Id"] = session[Constant.USER_ID].toString()

            val jsonObject = JsonObject()
            try {
                jsonObject.addProperty(valueOutfit!!._id, dataSize[valueSize-1])
                mViewModel.doAddSize(headers, jsonObject)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        } else {
            toast("Please choose both of outfit and size!!")
        }
    }

    private fun showDialogOutfit(){
        getPerlengkapanExist()

        val mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_category, null)

        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)

        alertDialogOutfit = mBuilder.create()
        alertDialogOutfit.window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )

        alertDialogOutfit.window!!.setDimAmount(0f)
        alertDialogOutfit.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));


        alertDialogOutfit = mBuilder.show()

        val tvTitle = alertDialogOutfit.findViewById<TextView>(R.id.dialog_title)
        val searchView = alertDialogOutfit.findViewById<SearchView>(R.id.search_view)
        val rvCategory = alertDialogOutfit.findViewById<RecyclerView>(R.id.rv_category)
        val linearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        outfitAdapter = OutfitAdapter(this, dataOutfit)

        tvTitle!!.text = "Outfit"

        rvCategory!!.layoutManager = linearLayoutManager

        rvCategory.itemAnimator = DefaultItemAnimator()!!
        rvCategory.adapter = outfitAdapter

        outfitAdapter.listener = this

        alertDialogOutfit.btn_close.setOnClickListener {
            alertDialogOutfit.dismiss()
        }

        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var textSearch = newText.toString()
                textSearch = textSearch.toLowerCase()
                var newList: MutableList<PerlengkapanEntity> = ArrayList()
                if (textSearch.isEmpty()) {
                    newList = dataOutfit.toMutableList()
                } else {
                    for (model in dataOutfit) {
                        val title: String = model.name.toLowerCase()
                        if (title.contains(textSearch)) {
                            newList.add(model)
                        }
                    }
                }
                outfitAdapter.setFilter(newList)

                return false
            }

        })
    }

    private fun showDialogCategory(type: String){
        val category = if (type == "category"){
            dataCategory
        } else {
            dataSubCategory
        }

        val mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_category, null)

        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)

        alertDialogCategory = mBuilder.create()
        alertDialogCategory.window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )

        alertDialogCategory.window!!.setDimAmount(0f)
        alertDialogCategory.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));


        alertDialogCategory = mBuilder.show()

        val tvTitle = alertDialogCategory.findViewById<TextView>(R.id.dialog_title)
        val searchView = alertDialogCategory.findViewById<SearchView>(R.id.search_view)
        val rvCategory = alertDialogCategory.findViewById<RecyclerView>(R.id.rv_category)
        val linearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        categoryAdapter = CategoryAdapter(this, category, type)

        tvTitle!!.text = if (type == "category"){
            "Category"
        } else {
            "Sub Category"
        }

        rvCategory!!.layoutManager = linearLayoutManager

        rvCategory.itemAnimator = DefaultItemAnimator()!!
        rvCategory.adapter = categoryAdapter

        categoryAdapter.listener = this

        alertDialogCategory.btn_close.setOnClickListener {
            alertDialogCategory.dismiss()
        }

        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var textSearch = newText.toString()
                textSearch = textSearch.toLowerCase()
                var newList: MutableList<KategoriEntity> = ArrayList()
                if (textSearch.isEmpty()) {
                    newList = category.toMutableList()
                } else {
                    for (model in category) {
                        val title: String = model.name.toLowerCase()
                        if (title.contains(textSearch)) {
                            newList.add(model)
                        }
                    }
                }
                categoryAdapter.setFilter(newList)

                return false
            }

        })
    }

    private fun setupAdapterSize(){
        val linearLayoutManager = GridLayoutManager(this@InputActivity, 4)
        radioSizeAdapter = RadioSizeAdapter(this@InputActivity, dataSize)

        rv_size.layoutManager = linearLayoutManager

        rv_size.itemAnimator = DefaultItemAnimator()!!
        rv_size.adapter = radioSizeAdapter

        radioSizeAdapter.listener = this
    }

    private fun resetData(){
        radioSizeAdapter.resetAdapter()

        dataSize.clear()
        radioSizeAdapter.notifyDataSetChanged()

        valueOutfit = null
        valueSize = 0
        valueCategory = ""
        valueSubCategory = ""

        tv_category.text = "Choose category"
        tv_sub_category.text = "Choose sub category"
        tv_outfit.text = "Choose outfit"
    }

    private fun onSuccessInputSize(state: SizeModel){
        val lastIndex = state.data.lastIndex
        val lastData = state.data[lastIndex]
        btn_save.revertAnimation{
            resetData()

            getPerlengkapanById(lastData)
        }
    }

    override fun onChanged(state: InputState?) {
        when(state){
            is InputState.OnSuccessInputSize -> {
                val status = state.sizeModel.status
                if (status == "200"){
                    onSuccessInputSize(state.sizeModel)
                }
            }

            is InputState.OnErrorState -> {
                btn_save.revertAnimation()
                toast("Failed add data, please try again later or login again!")
            }
        }
    }

    override fun onItemCategoryClicked(data: KategoriEntity, type: String) {
        when(type){
            "category" -> {
                valueCategory = data.name
                valueSubCategory = ""

                tv_category.text = data.name
                ly_sub_category.visibility = View.VISIBLE

                getSubCategory(data._id)

            }
            "sub" -> {
                valueSubCategory = data.name

                tv_sub_category.text = data.name
            }
        }

        valueOutfit = null
        tv_outfit.text = "Choose outfit"

        dataSize.clear()
        radioSizeAdapter.notifyDataSetChanged()

        alertDialogCategory.dismiss()
    }

    override fun onItemOutfitClicked(data: PerlengkapanEntity) {
        valueOutfit = data
        valueSize = 0

        tv_outfit.text = data.name

        dataSize.clear()
        dataSize.addAll(data.ukuran)
        radioSizeAdapter.resetAdapter()
        radioSizeAdapter.notifyDataSetChanged()

        alertDialogOutfit.dismiss()
    }

    override fun onItemSizeClicked(position: Int) {
        valueSize = position + 1
    }

    private fun onImageClick() {
        setupPermissionsCameraView({
            showImagePickerOptions()
        }, {
            showSettingsDialog()
        })
    }

    private fun loadImage(url: String){
        Log.d(TAG, "img cache url: $url")
        layout_no_image.visibility = View.GONE
        img_product.visibility = View.VISIBLE

        img_product.shapeAppearanceModel = img_product.shapeAppearanceModel
            .toBuilder()
            .setAllCorners(CornerFamily.ROUNDED, 16f)
            .build()

        Glide.with(this@InputActivity)
            .asBitmap()
            .load(url)
            .into(object : BitmapImageViewTarget(img_product){
                override fun setResource(resource: Bitmap?) {
                    super.setResource(resource)
                    img_product.setImageBitmap(resource)
                }
            })

        getCategoryRandom()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                try {
                    loadImage(uri!!.toString())
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }
    }

    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(
            this,
            object :
                ImagePickerActivity.PickerOptionListener {
                override fun onTakeCameraSelected() {
                    launchCameraIntent()
                }

                override fun onChooseGallerySelected() {
                    launchGalleryIntent()
                }
            })
    }

    private fun launchCameraIntent() {
        val intent = Intent(this@InputActivity, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_IMAGE_CAPTURE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000)

        startActivityForResult(intent,
            REQUEST_IMAGE
        )
    }

    private fun launchGalleryIntent() {
        val intent = Intent(this@InputActivity, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_GALLERY_IMAGE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent,
            REQUEST_IMAGE
        )
    }
}