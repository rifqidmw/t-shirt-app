package com.rifqidmw.tshirtapp.ui.login

import com.rifqidmw.tshirtapp.data.local.model.LoginModel
import com.rifqidmw.tshirtapp.data.local.model.SizeModel
import com.rifqidmw.tshirtapp.data.local.model.UserDetailModel

interface ILoginViewModel {
    fun successLogin(loginModel: LoginModel)
    fun successGetUserDetail(userDetailModel: UserDetailModel)
    fun successGetSize(sizeModel: SizeModel)
    fun errorLoad(t: Throwable)
}