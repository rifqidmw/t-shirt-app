package com.rifqidmw.tshirtapp.ui.main.fragment.home

import com.rifqidmw.novel_kun.utls.RxViewModel
import com.rifqidmw.tshirtapp.data.local.model.NewsModel
import com.rifqidmw.tshirtapp.data.local.model.SizeModel
import com.rifqidmw.tshirtapp.data.repo.NewsRepo
import com.rifqidmw.tshirtapp.data.repo.OutfitRepo
import com.rifqidmw.tshirtapp.utils.logD
import com.rifqidmw.tshirtapp.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HomeViewModel (val newsRepo: NewsRepo, val outfitRepo: OutfitRepo) : RxViewModel<HomeState>(), IHomeViewModel{
    override val TAG: String = HomeState::class.java.simpleName

    override fun successGetNews(newsModel: NewsModel) {
        logD(TAG,"success get news")
        state.value = HomeState.OnSuccessGetNews(newsModel)
    }

    override fun successGetSize(sizeModel: SizeModel) {
        logD(TAG,"success get size")
        state.value = HomeState.OnSuccessGetSize(sizeModel)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = HomeState.OnErrorState(t)
    }

    fun doGetNews(header: Map<String, String>) {
        logD(TAG,"do load news")
        launch {
            newsRepo.getNews(header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetNews,this::errorLoad)
        }
    }

    fun doGetSize(header: Map<String, String>) {
        logD(TAG,"do load size")
        launch {
            outfitRepo.getSize(header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetSize,this::errorLoad)
        }
    }
}