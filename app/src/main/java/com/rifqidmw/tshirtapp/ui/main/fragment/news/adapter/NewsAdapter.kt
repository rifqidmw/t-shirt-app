package com.rifqidmw.tshirtapp.ui.main.fragment.news.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mikhaellopez.circularimageview.CircularImageView
import com.rifqidmw.tshirtapp.R
import com.rifqidmw.tshirtapp.data.local.model.NewsModel
import com.rifqidmw.tshirtapp.ui.detailNews.DetailNewsActivity

class NewsAdapter(private val mContext: Context, private var dataList: MutableList<NewsModel.Data>)
    : RecyclerView.Adapter<NewsAdapter.MyViewHolder>() {

    private val TAG = NewsAdapter::class.java.simpleName
    private lateinit var itemView: View

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NewsAdapter.MyViewHolder {
        itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_news, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: NewsAdapter.MyViewHolder, position: Int) {
        val data = dataList[position]

        holder.tvTitle.text = data.title
        holder.tvDesc.text = data.content
        holder.tvDate.text = data.createdAt

        Glide.with(mContext).load(R.drawable.img_profile_ex).into(holder.imgUploader)

        holder.itemView.setOnClickListener {
            val intent = Intent(mContext, DetailNewsActivity::class.java)
            intent.putExtra("NEWS_TITLE", data.title)
            intent.putExtra("NEWS_CONTENT", data.content)
            intent.putExtra("NEWS_DATE", data.createdAt)
            mContext.startActivity(intent)
        }
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var tvTitle: TextView = itemView.findViewById(R.id.tv_title)
        internal var tvDate: TextView = itemView.findViewById(R.id.tv_date)
        internal var tvDesc: TextView = itemView.findViewById(R.id.tv_desc)
        internal var imgUploader: CircularImageView = itemView.findViewById(R.id.img_uploader)
    }

    fun updateData(data: ArrayList<NewsModel.Data>){
        this.dataList.addAll(data)

        notifyDataSetChanged()
    }
}