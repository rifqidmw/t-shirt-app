package com.rifqidmw.tshirtapp.ui.editProfile

import com.rifqidmw.tshirtapp.data.local.model.LoginModel
import com.rifqidmw.tshirtapp.data.local.model.RegisterModel
import com.rifqidmw.tshirtapp.data.local.model.UserDetailModel

interface IEditProfileViewModel {
    fun successEditProfile(userDetailModel: UserDetailModel)
    fun successLogin(loginModel: LoginModel)
    fun errorLoad(t: Throwable)
}