package com.rifqidmw.tshirtapp.ui.input.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rifqidmw.tshirtapp.R
import com.rifqidmw.tshirtapp.data.local.entity.PerlengkapanEntity

class OutfitAdapter(private val mContext: Context, private var dataList: MutableList<PerlengkapanEntity>)
    : RecyclerView.Adapter<OutfitAdapter.MyViewHolder>() {

    private val TAG = OutfitAdapter::class.java.simpleName
    private lateinit var itemView: View

    var listener: OutfitAdapterClickListener? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): OutfitAdapter.MyViewHolder {
        itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_kesatuan, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: OutfitAdapter.MyViewHolder, position: Int) {
        val data = dataList[position]

        holder.tvName.text = data.name

        holder.itemView.setOnClickListener {
            listener?.onItemOutfitClicked(data)
        }
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var tvName: TextView = itemView.findViewById(R.id.tv_name)
    }

    fun updateData(data: ArrayList<PerlengkapanEntity>){
        this.dataList.clear()
        this.dataList.addAll(data)

        notifyDataSetChanged()
    }

    fun setFilter(newList: MutableList<PerlengkapanEntity>) {
        dataList = java.util.ArrayList<PerlengkapanEntity>()
        dataList.addAll(newList)
        notifyDataSetChanged()
    }

}