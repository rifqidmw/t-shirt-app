package com.rifqidmw.tshirtapp.ui.editProfile

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.rifqidmw.tshirtapp.R
import com.rifqidmw.tshirtapp.data.local.MainDatabase
import com.rifqidmw.tshirtapp.data.local.Session
import com.rifqidmw.tshirtapp.data.local.entity.KesatuanEntity
import com.rifqidmw.tshirtapp.data.local.model.KesatuanModel
import com.rifqidmw.tshirtapp.data.local.model.LoginModel
import com.rifqidmw.tshirtapp.data.local.model.UserDetailModel
import com.rifqidmw.tshirtapp.ui.editProfile.adapter.KesatuanAdapter
import com.rifqidmw.tshirtapp.ui.editProfile.adapter.RecyclerViewClickListener
import com.rifqidmw.tshirtapp.ui.login.LoginActivity
import com.rifqidmw.tshirtapp.utils.Constant
import com.rifqidmw.tshirtapp.utils.logD
import com.rifqidmw.tshirtapp.utils.toast
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_edit_profile.et_email
import kotlinx.android.synthetic.main.activity_edit_profile.et_kesatuan
import kotlinx.android.synthetic.main.activity_edit_profile.et_layout_email
import kotlinx.android.synthetic.main.activity_edit_profile.et_layout_kesatuan
import kotlinx.android.synthetic.main.activity_edit_profile.et_layout_name
import kotlinx.android.synthetic.main.activity_edit_profile.et_layout_nrp
import kotlinx.android.synthetic.main.activity_edit_profile.et_layout_phone
import kotlinx.android.synthetic.main.activity_edit_profile.et_name
import kotlinx.android.synthetic.main.activity_edit_profile.et_nrp
import kotlinx.android.synthetic.main.activity_edit_profile.et_phone
import kotlinx.android.synthetic.main.activity_edit_profile.radio_gender
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.dialog_kesatuan.*
import org.json.JSONException
import org.koin.android.ext.android.inject
import java.util.HashMap

class EditProfileActivity : AppCompatActivity(), Observer<EditProfileState>, RecyclerViewClickListener {
    private val TAG: String = EditProfileActivity::class.java.simpleName
    private val session: Session by inject()
    private val mViewModel: EditProfileViewModel by inject()
    private val compositeDisposable = CompositeDisposable()
    private val mainDatabase: MainDatabase by inject()

    private var dataKesatuan: ArrayList<KesatuanEntity> = ArrayList()
    private var valueKesatuan: String = ""

    private lateinit var kesatuanAdapter: KesatuanAdapter
    private lateinit var alertDialog: AlertDialog
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        mViewModel.state.observe(this, this@EditProfileActivity)

        setupView()
    }

    private fun setupView(){
        et_name.setText(session[Constant.USERNAME])
        et_nrp.setText(session[Constant.NRP])
        et_email.setText(session[Constant.EMAIL])
        et_phone.setText(session[Constant.PHONE])
        et_kesatuan.setText(session[Constant.KESATUAN])
        getKesatuanById(session[Constant.KESATUAN]!!)

        if (session[Constant.JK] == "L"){
            radio_gender.check(R.id.rbLaki)
        } else {
            radio_gender.check(R.id.rbPerempuan)
        }

        et_kesatuan.setOnClickListener {
            showDialogKesatuan()
        }

        toolbar.setNavigationOnClickListener {
            onBackPressed()
            this@EditProfileActivity.finish()
        }

        btn_update.setOnClickListener {
            doUpdateProfile()
        }
    }

    private fun showDialogKesatuan(){
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_kesatuan, null)

        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)

        alertDialog = mBuilder.create()
        alertDialog.window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )

        alertDialog.window!!.setDimAmount(0f)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));


        alertDialog = mBuilder.show()

        progressBar = alertDialog.findViewById<ProgressBar>(R.id.progress_bar)!!
        progressBar.visibility = View.VISIBLE

        getKesatuan()

        val searchView = alertDialog.findViewById<SearchView>(R.id.search_view)
        val rvKesatuan = alertDialog.findViewById<RecyclerView>(R.id.rv_kesatuan)
        val linearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        kesatuanAdapter = KesatuanAdapter(this, dataKesatuan)

        rvKesatuan!!.layoutManager = linearLayoutManager

        rvKesatuan.itemAnimator = DefaultItemAnimator()!!
        rvKesatuan.adapter = kesatuanAdapter

        kesatuanAdapter.listener = this

        alertDialog.btn_close.setOnClickListener {
            alertDialog.dismiss()
        }

        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var textSearch = newText.toString()
                textSearch = textSearch.toLowerCase()
                var newList: MutableList<KesatuanEntity> = ArrayList()
                if (textSearch.isEmpty()) {
                    newList = dataKesatuan.toMutableList()
                } else {
                    for (model in dataKesatuan) {
                        val title: String = model.name.toLowerCase()
                        if (title.contains(textSearch)) {
                            newList.add(model)
                        }
                    }
                }
                kesatuanAdapter.setFilter(newList)

                return false
            }

        })
    }

    private fun doUpdateProfile(){
        val username = et_name.text.toString()
        val nrp = et_nrp.text.toString()
        val email = et_email.text.toString()
        val phone = et_phone.text.toString()
        val password = session[Constant.USER_PASSWORD]
        val genderId = radio_gender.checkedRadioButtonId
        val genderString = resources.getResourceEntryName(genderId)
        val gender = if (genderString == "rbLaki"){
            "L"
        } else {
            "W"
        }
        if (username.isNotEmpty() && nrp.isNotEmpty() && email.isNotEmpty() &&
            phone.isNotEmpty() && valueKesatuan.isNotEmpty()){

            btn_update.startAnimation()
            removeErrorStateEdittext()

            val headers = HashMap<String, String>()
            headers["Content-Type"] = "application/json"
            headers["X-Auth-Token"] = session[Constant.AUTH_TOKEN].toString()
            headers["X-User-Id"] = session[Constant.USER_ID].toString()

            val jsonObject = JsonObject()
            try {
                jsonObject.addProperty("username", username)
                jsonObject.addProperty("email", email)
                jsonObject.addProperty("password", password)
                jsonObject.addProperty("nrp", nrp)
                jsonObject.addProperty("phone", phone)
                jsonObject.addProperty("jenis_kelamin", gender)
                jsonObject.addProperty("kesatuan", valueKesatuan)

                mViewModel.doUpdateData(headers, jsonObject, session[Constant.USER_ID]!!)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        } else {
            if (username.isEmpty())
                et_layout_name.error = "Must be fill"
            else
                et_layout_name.isErrorEnabled = false
            if (nrp.isEmpty())
                et_layout_nrp.error = "Must be fill"
            else
                et_layout_nrp.isErrorEnabled = false
            if (email.isEmpty())
                et_layout_email.error = "Must be fill"
            else
                et_layout_email.isErrorEnabled = false
            if (phone.isEmpty())
                et_layout_phone.error = "Must be fill"
            else
                et_layout_phone.isErrorEnabled = false
            if (valueKesatuan.isEmpty())
                et_layout_kesatuan.error = "Must be fill"
            else
                et_layout_kesatuan.isErrorEnabled = false
        }
    }

    private fun doLogin(){
        val username = session[Constant.USERNAME]
        val password = session[Constant.USER_PASSWORD]
        val headers = HashMap<String, String>()

        headers["Content-Type"] = "application/json"

        val jsonObject = JsonObject()
        try {
            jsonObject.addProperty("username", username)
            jsonObject.addProperty("password", password)

            mViewModel.doLogin(headers, jsonObject)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun getKesatuan(){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getKesatuan()}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                progressBar.visibility = View.GONE
                dataKesatuan.clear()
                dataKesatuan.addAll(it)
                kesatuanAdapter.notifyDataSetChanged()
            },{}))
    }

    private fun getKesatuanById(id: String){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getKesatuanById(id)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                et_kesatuan.setText(it.name)
                valueKesatuan = it._id
            },{}))
    }

    override fun onChanged(state: EditProfileState?) {
        when(state){
            is EditProfileState.OnSuccessUpdateProfile -> {
                val status = state.userDetailModel.status
                if (status == "200"){
                    onSuccessUpdateProfile(state.userDetailModel.data)
                } else {
                    btn_update.revertAnimation()
                    toast("User update failed, please try again later!!")
                }

            }

            is EditProfileState.OnSuccessLogin -> {
                val status = state.loginModel.status
                if (status == "success"){
                    val data = state.loginModel.data
                    onSuccessReLogin(data)
                } else {
                    toast("Sorry, please re login !!")
                    startActivity(Intent(this@EditProfileActivity, LoginActivity::class.java))
                    finish()
                }
            }

            is EditProfileState.OnErrorState -> {
                btn_update.revertAnimation()
                toast("User update failed, please try again later or re login !!")
            }
        }
    }

    private fun onSuccessUpdateProfile(data: UserDetailModel.Data){
        session.save(Constant.USERNAME, data.username)
        session.save(Constant.NRP, data.profile.nrp)
        session.save(Constant.EMAIL, data.emails[0].address)
        session.save(Constant.EMAIL_VERIFIED, data.emails[0].verified)
        session.save(Constant.PHONE, data.profile.phone)
        session.save(Constant.JK, data.profile.jenis_kelamin)
        session.save(Constant.KESATUAN, data.profile.kesatuan)

        doLogin()
    }

    private fun onSuccessReLogin(data: LoginModel.Data){
        session.save(Constant.USER_ID, data.userId)
        session.save(Constant.AUTH_TOKEN, data.authToken)

        btn_update.revertAnimation()
        toast("Data profile has been updated!!")
    }

    private fun removeErrorStateEdittext(){
        et_layout_name.isErrorEnabled = false
        et_layout_nrp.isErrorEnabled = false
        et_layout_email.isErrorEnabled = false
        et_layout_phone.isErrorEnabled = false
        et_layout_kesatuan.isErrorEnabled = false
    }

    override fun onItemKesatuanClicked(data: KesatuanEntity) {
        alertDialog.dismiss()

        et_kesatuan.setText(data.name)
        valueKesatuan = data._id
    }
}