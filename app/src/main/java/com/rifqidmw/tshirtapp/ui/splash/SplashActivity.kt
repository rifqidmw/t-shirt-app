package com.rifqidmw.tshirtapp.ui.splash

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.ViewGroup
import android.view.WindowInsets
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.Button
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.rifqidmw.tshirtapp.R
import com.rifqidmw.tshirtapp.data.local.MainDatabase
import com.rifqidmw.tshirtapp.data.local.Session
import com.rifqidmw.tshirtapp.data.local.entity.KategoriEntity
import com.rifqidmw.tshirtapp.data.local.entity.KesatuanEntity
import com.rifqidmw.tshirtapp.data.local.entity.PerlengkapanEntity
import com.rifqidmw.tshirtapp.data.local.model.KategoriModel
import com.rifqidmw.tshirtapp.data.local.model.KesatuanModel
import com.rifqidmw.tshirtapp.data.local.model.PerlengkapanModel
import com.rifqidmw.tshirtapp.ui.login.LoginActivity
import com.rifqidmw.tshirtapp.ui.main.MainActivity
import com.rifqidmw.tshirtapp.utils.Constant
import com.rifqidmw.tshirtapp.utils.toast
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_splash.*
import org.koin.android.ext.android.inject

class SplashActivity : AppCompatActivity(), Observer<SplashState> {
    private val session: Session by inject()
    private val mViewModel: SplashViewModel by inject()
    private val compositeDisposable = CompositeDisposable()
    private val mainDatabase: MainDatabase by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        mViewModel.state.observe(this, this@SplashActivity)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }

        Glide.with(this@SplashActivity).load(R.drawable.img_logo).into(img_logo)

        val topAnim = AnimationUtils.loadAnimation(this@SplashActivity ,
            R.anim.top_animation
        )
        val bottomAnim = AnimationUtils.loadAnimation(this@SplashActivity,
            R.anim.bottom_animation
        )

        img_logo.animation = topAnim
        tv_title.animation = bottomAnim
        tv_desc.animation = bottomAnim
        progress_bar.animation = bottomAnim

        mViewModel.doGetKesatuan()
    }

    private fun handleIntent(){
        val pair1 = Pair.create<View, String>(img_logo, "logo_image")
        val pair2 = Pair.create<View, String>(tv_title, "logo_title")

        if (session[Constant.USER_ID] != null){
            val intent = Intent(this@SplashActivity, MainActivity::class.java)
            startActivity(intent)
            this@SplashActivity.finish()
        } else {
            val intent = Intent(this@SplashActivity, LoginActivity::class.java)

            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pair1, pair2)
            startActivity(intent, options.toBundle())
            this@SplashActivity.finish()
        }
    }

    private fun onSuccessGetKesatuan(state: KesatuanModel){
        val data = state.data

        if (data.isNotEmpty()){
            for (i in data.indices){
                val _id = data[i]._id
                val name = data[i].name
                val createdBy = data[i].createdBy
                val createdAt = data[i].createdAt

                addKesatuan(KesatuanEntity(_id, name, createdBy, createdAt))
            }

            mViewModel.doGetPerlengkapan()
        } else {
            mViewModel.doGetPerlengkapan()
        }
    }

    private fun onSuccessGetPerlengkapan(state: PerlengkapanModel){
        val data = state.data

        if (data.isNotEmpty()){
            for (i in data.indices){
                val _id = data[i]._id
                val name = data[i].name
                val ukuran = data[i].ukuran
                val imageurl = data[i].imageurl
                val category = data[i].categori
                val subCategory = data[i].subcategori

                addPerlengkapan(PerlengkapanEntity(_id, name, imageurl, ukuran, category, subCategory))
            }

            mViewModel.doGetKategori()
        } else {
            mViewModel.doGetKategori()
        }
    }

    private fun onSuccessGetKategori(state: KategoriModel){
        val data = state.data

        if (data.isNotEmpty()){
            for (i in data.indices){
                val _id = data[i]._id
                val name = data[i].name
                val parent = data[i].parent

                addKategori(KategoriEntity(_id, name, parent))
            }

            Handler(Looper.getMainLooper()).postDelayed({
                handleIntent()
            }, 1000)
        } else {
            Handler(Looper.getMainLooper()).postDelayed({
                handleIntent()
            }, 1000)

        }
    }

    private fun addKesatuan(kesatuanModel: KesatuanEntity){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().addKesatuan(kesatuanModel)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

            },{
                toast("There's problem in local system for code K1, please contant developer to fix it!!")
            }))
    }

    private fun addPerlengkapan(perlengkapanModel: PerlengkapanEntity){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().addPerlengkapan(perlengkapanModel)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

            },{
                toast("There's problem in local system for code P, please contant developer to fix it!!")
            }))
    }

    private fun addKategori(kategoriModel: KategoriEntity){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().addKategori(kategoriModel)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

            },{
                toast("There's problem in local system for code K2, please contant developer to fix it!!")
            }))
    }

    override fun onChanged(state: SplashState?) {
        when(state){
            is SplashState.OnSuccessGetKesatuan -> {
                val status = state.kesatuanModel.status
                if (status == "200"){
                    onSuccessGetKesatuan(state.kesatuanModel)
                }
            }

            is SplashState.OnSuccessGetPerlengkapan -> {
                val status = state.perlengkapanModel.status
                if (status == "200"){
                    onSuccessGetPerlengkapan(state.perlengkapanModel)
                }
            }

            is SplashState.OnSuccessGetKategori -> {
                val status = state.kategoriModel.status
                if (status == "200"){
                    onSuccessGetKategori(state.kategoriModel)
                }
            }

            is SplashState.OnErrorState -> {
                toast("Opps, there is something wrong! Please try again later!")
            }
        }
    }
}