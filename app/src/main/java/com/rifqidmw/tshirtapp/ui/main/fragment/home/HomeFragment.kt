package com.rifqidmw.tshirtapp.ui.main.fragment.home

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mikhaellopez.circularimageview.CircularImageView
import com.rifqidmw.tshirtapp.R
import com.rifqidmw.tshirtapp.data.local.MainDatabase
import com.rifqidmw.tshirtapp.data.local.Session
import com.rifqidmw.tshirtapp.data.local.model.NewsModel
import com.rifqidmw.tshirtapp.ui.main.fragment.home.adapter.RecentNewsAdapter
import com.rifqidmw.tshirtapp.ui.main.fragment.news.NewsFragment
import com.rifqidmw.tshirtapp.utils.Constant
import com.rifqidmw.tshirtapp.utils.toast
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.ext.android.inject
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment(), Observer<HomeState> {
    private val session: Session by inject()
    private val mViewModel: HomeViewModel by inject()
    private val compositeDisposable = CompositeDisposable()
    private val mainDatabase: MainDatabase by inject()

    private var param1: String? = null
    private var param2: String? = null
    private var sizeTotal: Int = 0
    private var dataItem: ArrayList<NewsModel.Data> = ArrayList()

    private lateinit var recentNewsAdapter: RecentNewsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            /*param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)*/
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        mViewModel.state.observe(viewLifecycleOwner,this)

        setupView(view)
        getDataNews()
        getDataSizeLocal()
//        getDataSize()

        return view
    }

    @SuppressLint("UseCompatLoadingForDrawables", "SetTextI18n")
    fun setupView(view: View){

        val btnSeeAll = view.findViewById(R.id.btn_see_all) as TextView
        val tvUsername = view.findViewById(R.id.tv_username) as TextView
        val imgProfile = view.findViewById(R.id.img_profile) as CircularImageView

        tvUsername.text = "Hi, ${session[Constant.USERNAME]}"
        Glide.with(this).load(R.drawable.img_profile_ex).into(imgProfile)

        btnSeeAll.setOnClickListener {
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.replace(R.id.content_panel, NewsFragment.newInstance())
            transaction.disallowAddToBackStack()
            transaction.commit()

            requireActivity().bottomNavigationView.menu.findItem(R.id.menu_news).isChecked = true
        }

        //adapter
        val rvRecent = view.findViewById(R.id.rv_recent) as RecyclerView
        val linearLayoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        recentNewsAdapter = RecentNewsAdapter(requireContext(), dataItem)

        rvRecent.layoutManager = linearLayoutManager

        rvRecent.itemAnimator = DefaultItemAnimator()!!
        rvRecent.adapter = recentNewsAdapter
    }

    private fun getDataNews(){
        val headers = HashMap<String, String>()
        headers["Content-Type"] = "application/json"
        headers["X-Auth-Token"] = session[Constant.AUTH_TOKEN].toString()
        headers["X-User-Id"] = session[Constant.USER_ID].toString()

        mViewModel.doGetNews(headers)
    }

    private fun getDataSize(){
        val headers = HashMap<String, String>()
        headers["Content-Type"] = "application/json"
        headers["X-Auth-Token"] = session[Constant.AUTH_TOKEN].toString()
        headers["X-User-Id"] = session[Constant.USER_ID].toString()

        mViewModel.doGetSize(headers)
    }

    private fun getDataSizeLocal(){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getPerlengkapan()}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                sizeTotal = it.size
            },{},{ getDataOutfitLocal() }))
    }

    private fun getDataOutfitLocal(){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getOutfit(session[Constant.USER_ID]!!)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                val outfitSize = it.size

                var percentage = (outfitSize.toDouble() / sizeTotal) * 100f
                tv_measured_precent.text = "${percentage.toInt()}%"
                tv_ismeasured.text = "$outfitSize"
                tv_notmeasured.text = "$sizeTotal"
            },{}))
    }


    companion object {

        @JvmStatic
        fun newInstance() =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    /*putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)*/
                }
            }
    }

    override fun onChanged(state: HomeState?) {
        when(state){
            is HomeState.OnSuccessGetNews -> {
                val status = state.newsModel.status
                val msg = state.newsModel.msg
                if (status == "200"){
                    val data = state.newsModel.data
                    recentNewsAdapter.updateData(data)
                } else {
                    requireActivity().toast(msg)
                }
            }

            is HomeState.OnSuccessGetSize -> {
                val status = state.sizeModel.status

                if (status == "200"){
                    val data = state.sizeModel.data

                    var percentage = (data.size.toDouble() / sizeTotal) * 100f
                    tv_measured_precent.text = "${percentage.toInt()}%"
                    tv_ismeasured.text = "${data.size}"
                    tv_notmeasured.text = "$sizeTotal"
                }
            }

            is HomeState.OnErrorState -> {
                requireActivity().toast("Failed load data, try again later or re login")
            }
        }
    }
}