package com.rifqidmw.tshirtapp.ui.main.fragment.home

import com.rifqidmw.tshirtapp.data.local.model.NewsModel
import com.rifqidmw.tshirtapp.data.local.model.SizeModel

interface IHomeViewModel {
    fun successGetNews(newsModel: NewsModel)
    fun successGetSize(sizeModel: SizeModel)
    fun errorLoad(t: Throwable)
}