package com.rifqidmw.tshirtapp.ui.login

import com.google.gson.JsonObject
import com.rifqidmw.novel_kun.utls.RxViewModel
import com.rifqidmw.tshirtapp.data.local.model.LoginModel
import com.rifqidmw.tshirtapp.data.local.model.SizeModel
import com.rifqidmw.tshirtapp.data.local.model.UserDetailModel
import com.rifqidmw.tshirtapp.data.repo.OutfitRepo
import com.rifqidmw.tshirtapp.data.repo.UserRepo
import com.rifqidmw.tshirtapp.utils.logD
import com.rifqidmw.tshirtapp.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LoginViewModel (val userRepo: UserRepo, val outfitRepo: OutfitRepo): RxViewModel<LoginState>(), ILoginViewModel {
    override val TAG: String = LoginState::class.java.simpleName

    override fun successLogin(loginModel: LoginModel) {
        logD(TAG,"success login")
        state.value = LoginState.OnSuccessLogin(loginModel)
    }

    override fun successGetUserDetail(userDetailModel: UserDetailModel) {
        logD(TAG,"success get user detail")
        state.value = LoginState.OnSuccessGetUserDetail(userDetailModel)
    }

    override fun successGetSize(sizeModel: SizeModel) {
        logD(TAG,"success get size")
        state.value = LoginState.OnSuccessGetSize(sizeModel)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = LoginState.OnErrorState(t)
    }

    fun doLogin(header:Map<String, String>,data: JsonObject) {
        logD(TAG,"do login start")
        launch {

            userRepo.doLogin(header, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successLogin) {
                    errorLoad(t = it)
                }
        }
    }

    fun getUserDetail(header:Map<String, String>, userId: String) {
        logD(TAG,"do login start")
        launch {

            userRepo.getUserDetail(header, userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetUserDetail) {
                    errorLoad(t = it)
                }
        }
    }

    fun doGetSize(header: Map<String, String>) {
        logD(TAG,"do load size")
        launch {
            outfitRepo.getSize(header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetSize,this::errorLoad)
        }
    }
}