package com.rifqidmw.tshirtapp.ui.main.fragment.Outfit.adapter

import android.view.View
import com.rifqidmw.tshirtapp.data.local.model.KesatuanModel

interface RadioSizeAdapterClickListener {

    fun onItemSizeClicked(position: Int)

}