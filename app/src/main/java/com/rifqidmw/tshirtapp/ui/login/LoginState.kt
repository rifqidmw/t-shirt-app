package com.rifqidmw.tshirtapp.ui.login

import com.rifqidmw.tshirtapp.data.local.model.LoginModel
import com.rifqidmw.tshirtapp.data.local.model.SizeModel
import com.rifqidmw.tshirtapp.data.local.model.UserDetailModel
import com.rifqidmw.tshirtapp.ui.main.fragment.Outfit.OutfitState

sealed class LoginState {
    data class OnSuccessLogin(val loginModel: LoginModel): LoginState()
    data class OnSuccessGetUserDetail(val userDetailModel: UserDetailModel): LoginState()
    data class OnSuccessGetSize(val sizeModel: SizeModel): LoginState()
    data class OnErrorState(val t: Throwable): LoginState()
}