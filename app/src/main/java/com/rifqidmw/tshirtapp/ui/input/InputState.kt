package com.rifqidmw.tshirtapp.ui.input

import com.rifqidmw.tshirtapp.data.local.model.SizeModel

sealed class InputState {
    data class OnSuccessInputSize(val sizeModel: SizeModel) : InputState()
    data class OnErrorState(val t: Throwable): InputState()
}