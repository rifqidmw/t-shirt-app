package com.rifqidmw.tshirtapp.ui.splash

import com.rifqidmw.tshirtapp.data.local.model.KategoriModel
import com.rifqidmw.tshirtapp.data.local.model.KesatuanModel
import com.rifqidmw.tshirtapp.data.local.model.PerlengkapanModel

interface ISplashViewModel {
    fun successGetKesatuan(kesatuanModel: KesatuanModel)
    fun successGetPerlengkapan(perlengkapanModel: PerlengkapanModel)
    fun successGetKategori(kategoriModel: KategoriModel)
    fun errorLoad(t: Throwable)
}