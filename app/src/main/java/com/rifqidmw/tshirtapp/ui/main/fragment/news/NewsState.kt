package com.rifqidmw.tshirtapp.ui.main.fragment.news

import com.rifqidmw.tshirtapp.data.local.model.NewsModel
import com.utsman.recycling.extentions.Recycling

sealed class NewsState {
    data class OnSuccessGetNews(val newsModel: NewsModel) : NewsState()
    data class OnErrorState(val t: Throwable): NewsState()
}