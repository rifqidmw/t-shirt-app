package com.rifqidmw.tshirtapp.ui.main.fragment.news

import com.google.gson.JsonObject
import com.rifqidmw.novel_kun.utls.RxViewModel
import com.rifqidmw.tshirtapp.data.local.model.NewsModel
import com.rifqidmw.tshirtapp.data.repo.NewsRepo
import com.rifqidmw.tshirtapp.utils.logD
import com.rifqidmw.tshirtapp.utils.logE
import com.utsman.recycling.extentions.Recycling
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class NewsViewModel (val newsRepo: NewsRepo): RxViewModel<NewsState>(), INewsViewModel {
    override val TAG: String = NewsState::class.java.simpleName

    override fun successGetNews(newsModel: NewsModel) {
        logD(TAG,"success get news")
        state.value = NewsState.OnSuccessGetNews(newsModel)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = NewsState.OnErrorState(t)
    }

    fun doGetNews(header: Map<String, String>) {
        logD(TAG,"do load news")
        launch {
            newsRepo.getNews(header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetNews,this::errorLoad)
        }
    }
}