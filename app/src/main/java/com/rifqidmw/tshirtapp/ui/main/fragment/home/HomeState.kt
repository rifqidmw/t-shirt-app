package com.rifqidmw.tshirtapp.ui.main.fragment.home

import com.rifqidmw.tshirtapp.data.local.model.NewsModel
import com.rifqidmw.tshirtapp.data.local.model.SizeModel
import com.rifqidmw.tshirtapp.ui.main.fragment.Outfit.OutfitState

sealed class HomeState {
    data class OnSuccessGetNews(val newsModel: NewsModel) : HomeState()
    data class OnSuccessGetSize(val sizeModel: SizeModel): HomeState()
    data class OnErrorState(val t: Throwable): HomeState()
}