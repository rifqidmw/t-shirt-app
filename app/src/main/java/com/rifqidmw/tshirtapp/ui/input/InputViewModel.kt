package com.rifqidmw.tshirtapp.ui.input

import com.google.gson.JsonObject
import com.rifqidmw.novel_kun.utls.RxViewModel
import com.rifqidmw.tshirtapp.data.local.model.SizeModel
import com.rifqidmw.tshirtapp.data.repo.InputRepo
import com.rifqidmw.tshirtapp.ui.login.LoginState
import com.rifqidmw.tshirtapp.utils.logD
import com.rifqidmw.tshirtapp.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class InputViewModel (val inputRepo: InputRepo) : RxViewModel<InputState>(), IInputViewModel {
    override val TAG: String = InputState::class.java.simpleName
    override fun successAddSize(sizeModel: SizeModel) {
        logD(TAG,"success login")
        state.value = InputState.OnSuccessInputSize(sizeModel)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = InputState.OnErrorState(t)
    }

    fun doAddSize(header:Map<String, String>,data: JsonObject) {
        logD(TAG,"do add size: $data")
        launch {

            inputRepo.doAddSize(header, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successAddSize) {
                    errorLoad(t = it)
                }
        }
    }
}