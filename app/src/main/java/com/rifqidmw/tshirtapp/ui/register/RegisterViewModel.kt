package com.rifqidmw.tshirtapp.ui.register

import com.google.gson.JsonObject
import com.rifqidmw.novel_kun.utls.RxViewModel
import com.rifqidmw.tshirtapp.data.local.model.KesatuanModel
import com.rifqidmw.tshirtapp.data.local.model.RegisterModel
import com.rifqidmw.tshirtapp.data.repo.UserRepo
import com.rifqidmw.tshirtapp.utils.logD
import com.rifqidmw.tshirtapp.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RegisterViewModel (val userRepo: UserRepo): RxViewModel<RegisterState>(), IRegisterViewModel {
    override val TAG: String = RegisterState::class.java.simpleName

    override fun successRegister(registerModel: RegisterModel) {
        logD(TAG,"success do register")
        state.value = RegisterState.OnSuccessRegister(registerModel)
    }

    override fun successGetKesatuan(kesatuanModel: KesatuanModel) {
        logD(TAG,"success get kesatuan")
        state.value = RegisterState.OnSuccessGetKesatuan(kesatuanModel)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = RegisterState.OnErrorState(t)
    }

    fun doRegister(data: JsonObject) {
        logD(TAG,"do login start")
        launch {

            userRepo.doRegister(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successRegister) {
                    errorLoad(t = it)
                }
        }
    }

    fun getKesatuan() {
        logD(TAG,"do login start")
        launch {

            userRepo.getKesatuan()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetKesatuan) {
                    errorLoad(t = it)
                }
        }
    }
}