package com.rifqidmw.tshirtapp.ui.input

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.rifqidmw.tshirtapp.R
import com.rifqidmw.tshirtapp.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_input_success.*

class InputSuccessActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_input_success)

        btn_back.setOnClickListener {
            val intent = Intent(this@InputSuccessActivity, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            this@InputSuccessActivity.finish()
        }

        btn_again.setOnClickListener {
            onBackPressed()
            this@InputSuccessActivity.finish()
        }
    }
}