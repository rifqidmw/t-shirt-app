package com.rifqidmw.tshirtapp.ui.register

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.WindowInsets
import android.view.WindowManager
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.rifqidmw.tshirtapp.R
import com.rifqidmw.tshirtapp.data.local.MainDatabase
import com.rifqidmw.tshirtapp.data.local.entity.KesatuanEntity
import com.rifqidmw.tshirtapp.data.local.model.KesatuanModel
import com.rifqidmw.tshirtapp.ui.login.LoginActivity
import com.rifqidmw.tshirtapp.ui.register.adapter.KesatuanAdapter
import com.rifqidmw.tshirtapp.ui.register.adapter.RecyclerViewClickListener
import com.rifqidmw.tshirtapp.utils.logD
import com.rifqidmw.tshirtapp.utils.toast
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.dialog_kesatuan.*
import org.json.JSONException
import org.koin.android.ext.android.inject
import java.util.HashMap

class RegisterActivity : AppCompatActivity(), Observer<RegisterState>, RecyclerViewClickListener {
    private lateinit var kesatuanAdapter: KesatuanAdapter
    private lateinit var progressBar: ProgressBar
    private lateinit var alertDialog: AlertDialog

    private var dataKesatuan: ArrayList<KesatuanEntity> = ArrayList()

    private val mViewModel: RegisterViewModel by inject()
    private val compositeDisposable = CompositeDisposable()
    private val mainDatabase: MainDatabase by inject()

    private var valueKesatuan: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        mViewModel.state.observe(this, this@RegisterActivity)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }

        et_kesatuan.setOnClickListener {
            showDialogKesatuan()
        }

        btn_signup.setOnClickListener {
            doRegister()
        }

        btn_signin.setOnClickListener {
            onBackPressed()
            this@RegisterActivity.finish()
        }
    }

    private fun doRegister(){
        val username = et_name.text.toString()
        val nrp = et_nrp.text.toString()
        val email = et_email.text.toString()
        val phone = et_phone.text.toString()
        val password = et_password.text.toString()
        val repassword = et_repassword.text.toString()

        val genderId = radio_gender.checkedRadioButtonId
        val genderString = resources.getResourceEntryName(genderId)
        val gender = if (genderString == "rbLaki"){
            "L"
        } else {
            "W"
        }
        if (username.isNotEmpty() && nrp.isNotEmpty() && email.isNotEmpty() &&
            phone.isNotEmpty() && valueKesatuan.isNotEmpty() && password.isNotEmpty() &&
            repassword.isNotEmpty()){

            if (password == repassword){
                btn_signup.startAnimation()
                removeErrorStateEdittext()

                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/json"

                val jsonObject = JsonObject()
                try {
                    jsonObject.addProperty("username", username)
                    jsonObject.addProperty("email", email)
                    jsonObject.addProperty("password", password)
                    jsonObject.addProperty("nrp", nrp)
                    jsonObject.addProperty("phone", phone)
                    jsonObject.addProperty("jenis_kelamin", gender)
                    jsonObject.addProperty("kesatuan", valueKesatuan)

                    mViewModel.doRegister(jsonObject)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            } else {
                et_repassword.setText("")
                et_layout_repassword.error = "Re-Password must be same with Password"
            }
        } else {
            if (username.isEmpty())
                et_layout_name.error = "Must be fill"
            else
                et_layout_name.isErrorEnabled = false
            if (nrp.isEmpty())
                et_layout_nrp.error = "Must be fill"
            else
                et_layout_nrp.isErrorEnabled = false
            if (email.isEmpty())
                et_layout_email.error = "Must be fill"
            else
                et_layout_email.isErrorEnabled = false
            if (phone.isEmpty())
                et_layout_phone.error = "Must be fill"
            else
                et_layout_phone.isErrorEnabled = false
            if (valueKesatuan.isEmpty())
                et_layout_kesatuan.error = "Must be fill"
            else
                et_layout_kesatuan.isErrorEnabled = false
            if (password.isEmpty())
                et_layout_password.error = "Must be fill"
            else
                et_layout_password.isErrorEnabled = false
            if (repassword.isEmpty())
                et_layout_repassword.error = "Must be fill"
            else
                et_layout_repassword.isErrorEnabled = false
        }
    }

    private fun showDialogKesatuan(){
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_kesatuan, null)

        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)

        alertDialog = mBuilder.create()
        alertDialog.window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )

        alertDialog.window!!.setDimAmount(0f)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));


        alertDialog = mBuilder.show()

        progressBar = alertDialog.findViewById<ProgressBar>(R.id.progress_bar)!!
        progressBar.visibility = View.VISIBLE

        getKesatuan()

        val searchView = alertDialog.findViewById<SearchView>(R.id.search_view)
        val rvKesatuan = alertDialog.findViewById<RecyclerView>(R.id.rv_kesatuan)
        val linearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        kesatuanAdapter = KesatuanAdapter(this, dataKesatuan)

        rvKesatuan!!.layoutManager = linearLayoutManager

        rvKesatuan.itemAnimator = DefaultItemAnimator()!!
        rvKesatuan.adapter = kesatuanAdapter

        kesatuanAdapter.listener = this

        alertDialog.btn_close.setOnClickListener {
            alertDialog.dismiss()
        }

        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var textSearch = newText.toString()
                textSearch = textSearch.toLowerCase()
                var newList: MutableList<KesatuanEntity> = ArrayList()
                if (textSearch.isEmpty()) {
                    newList = dataKesatuan.toMutableList()
                } else {
                    for (model in dataKesatuan) {
                        val title: String = model.name.toLowerCase()
                        if (title.contains(textSearch)) {
                            newList.add(model)
                        }
                    }
                }
                kesatuanAdapter.setFilter(newList)

                return false
            }

        })
    }

    private fun getKesatuan(){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getKesatuan()}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                progressBar.visibility = View.GONE
                dataKesatuan.clear()
                dataKesatuan.addAll(it)
                kesatuanAdapter.notifyDataSetChanged()
            },{}))
    }

    private fun removeErrorStateEdittext(){
        et_layout_name.isErrorEnabled = false
        et_layout_nrp.isErrorEnabled = false
        et_layout_email.isErrorEnabled = false
        et_layout_phone.isErrorEnabled = false
        et_layout_kesatuan.isErrorEnabled = false
        et_layout_password.isErrorEnabled = false
        et_layout_repassword.isErrorEnabled = false
    }

    override fun onChanged(state: RegisterState?) {
        when(state){
            is RegisterState.OnSuccessRegister -> {
                val status = state.registerModel.status
                val msg = state.registerModel.msg
                if (status == "200"){
                    btn_signup.revertAnimation{
                        toast("Registration success")
                        val intent = Intent(this@RegisterActivity, LoginActivity::class.java)
                        startActivity(intent)
                        this@RegisterActivity.finish()
                    }
                } else {
                    btn_signup.revertAnimation()
                    toast(msg)
                }
            }

            is RegisterState.OnSuccessGetKesatuan -> {

            }

            is RegisterState.OnErrorState -> {
                btn_signup.revertAnimation()
                toast(state.t.toString())
            }
        }
    }

    override fun onItemKesatuanClicked(data: KesatuanEntity) {
        logD("kesatuan", data.name)

        alertDialog.dismiss()
        et_kesatuan.setText(data.name)
        valueKesatuan = data._id
    }
}