package com.rifqidmw.tshirtapp.ui.register

import com.rifqidmw.tshirtapp.data.local.model.KesatuanModel
import com.rifqidmw.tshirtapp.data.local.model.RegisterModel

interface IRegisterViewModel {
    fun successRegister(registerModel: RegisterModel)
    fun successGetKesatuan(kesatuanModel: KesatuanModel)
    fun errorLoad(t: Throwable)
}