package com.rifqidmw.tshirtapp.ui.splash

import com.rifqidmw.novel_kun.utls.RxViewModel
import com.rifqidmw.tshirtapp.data.local.model.KategoriModel
import com.rifqidmw.tshirtapp.data.local.model.KesatuanModel
import com.rifqidmw.tshirtapp.data.local.model.PerlengkapanModel
import com.rifqidmw.tshirtapp.data.repo.SetupRepo
import com.rifqidmw.tshirtapp.ui.login.LoginState
import com.rifqidmw.tshirtapp.utils.logD
import com.rifqidmw.tshirtapp.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SplashViewModel (val setupRepo: SetupRepo) : RxViewModel<SplashState>(), ISplashViewModel {
    override val TAG: String = SplashState::class.java.simpleName

    override fun successGetKesatuan(kesatuanModel: KesatuanModel) {
        logD(TAG,"success get kesatuan")
        state.value = SplashState.OnSuccessGetKesatuan(kesatuanModel)
    }

    override fun successGetPerlengkapan(perlengkapanModel: PerlengkapanModel) {
        logD(TAG,"success get perlengkapan")
        state.value = SplashState.OnSuccessGetPerlengkapan(perlengkapanModel)
    }

    override fun successGetKategori(kategoriModel: KategoriModel) {
        logD(TAG,"success get kategori")
        state.value = SplashState.OnSuccessGetKategori(kategoriModel)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = SplashState.OnErrorState(t)
    }

    fun doGetKesatuan() {
        logD(TAG,"do load kesatuan")
        launch {
            setupRepo.getKesatuan()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetKesatuan,this::errorLoad)
        }
    }

    fun doGetPerlengkapan() {
        logD(TAG,"do load perlengkapan")
        launch {
            setupRepo.getPerlengkapan()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetPerlengkapan,this::errorLoad)
        }
    }

    fun doGetKategori() {
        logD(TAG,"do load kategori")
        launch {
            setupRepo.getKategori()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetKategori,this::errorLoad)
        }
    }

}