package com.rifqidmw.tshirtapp.ui.login

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowInsets
import android.view.WindowManager
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import com.rifqidmw.tshirtapp.R
import com.rifqidmw.tshirtapp.data.local.MainDatabase
import com.rifqidmw.tshirtapp.data.local.Session
import com.rifqidmw.tshirtapp.data.local.entity.KesatuanEntity
import com.rifqidmw.tshirtapp.data.local.entity.OutfitEntity
import com.rifqidmw.tshirtapp.data.local.model.LoginModel
import com.rifqidmw.tshirtapp.data.local.model.SizeModel
import com.rifqidmw.tshirtapp.ui.register.RegisterActivity
import com.rifqidmw.tshirtapp.ui.main.MainActivity
import com.rifqidmw.tshirtapp.utils.Constant
import com.rifqidmw.tshirtapp.utils.logD
import com.rifqidmw.tshirtapp.utils.logE
import com.rifqidmw.tshirtapp.utils.toast
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.btn_signin
import kotlinx.android.synthetic.main.activity_login.btn_signup
import kotlinx.android.synthetic.main.activity_login.logo_desc
import kotlinx.android.synthetic.main.activity_login.logo_image
import kotlinx.android.synthetic.main.activity_login.logo_title
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONException
import org.koin.android.ext.android.inject
import java.util.HashMap

class LoginActivity : AppCompatActivity(), Observer<LoginState> {
    private val TAG: String = LoginActivity::class.java.simpleName

    private val session: Session by inject()
    private val mViewModel: LoginViewModel by inject()
    private val compositeDisposable = CompositeDisposable()
    private val mainDatabase: MainDatabase by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mViewModel.state.observe(this, this@LoginActivity)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }

        Glide.with(this@LoginActivity).load(R.drawable.img_logo).into(logo_image)

        btn_signin.setOnClickListener {
            doLogin()
        }

        btn_signup.setOnClickListener {
            val pair1 = Pair.create<View, String>(logo_image, "logo_image")
            val pair2 = Pair.create<View, String>(logo_title, "logo_title")
            val pair3 = Pair.create<View, String>(logo_desc, "logo_desc")
            val pair4 = Pair.create<View, String>(tv_layout_username, "trans_email")
            val pair5 = Pair.create<View, String>(tv_layout_password, "trans_password")
            val pair6 = Pair.create<View, String>(btn_signin, "trans_button_1")
            val pair7 = Pair.create<View, String>(btn_signup, "trans_button_1")

            val intent = Intent(this@LoginActivity, RegisterActivity::class.java)

            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pair1, pair2, pair3)
            startActivity(intent, options.toBundle())
        }
    }

    private fun doLogin(){
        val username = tv_username.text.toString()?:""
        val password = tv_password.text.toString()?:""
        if (username.isNotEmpty() && password.isNotEmpty()){
            btn_signin.startAnimation()

            val headers = HashMap<String, String>()
            headers["Content-Type"] = "application/json"

            val jsonObject = JsonObject()
            try {
                jsonObject.addProperty("username", username)
                jsonObject.addProperty("password", password)

                mViewModel.doLogin(headers, jsonObject)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        } else {
            if (username.isEmpty()){
                tv_layout_username.error = "Must be fill"
            }

            if (password.isEmpty()){
                tv_layout_password.error = "Must be fill"
            }
        }
    }

    private fun getUserDetail(data: LoginModel.Data){
        val authToken = data.authToken
        val userId = data.userId
        val headers = HashMap<String, String>()
        headers["Content-Type"] = "application/json"
        headers["X-Auth-Token"] = authToken
        headers["X-User-Id"] = userId

        mViewModel.getUserDetail(headers, userId)
    }

    private fun getDataSize(){
        val headers = HashMap<String, String>()
        headers["Content-Type"] = "application/json"
        headers["X-Auth-Token"] = session[Constant.AUTH_TOKEN].toString()
        headers["X-User-Id"] = session[Constant.USER_ID].toString()

        mViewModel.doGetSize(headers)
    }

    override fun onChanged(state: LoginState?) {
        when(state){
            is LoginState.OnSuccessLogin -> {
                val status = state.loginModel.status

                if (status == "success"){
                    val data = state.loginModel.data

                    session.save(Constant.AUTH_TOKEN, data.authToken)
                    session.save(Constant.USER_ID, data.userId)
                    session.save(Constant.USER_PASSWORD, tv_password.text.toString())

                    getUserDetail(data)
                } else {
                    toast("Login failed, please try again later !!!")
                }
            }

            is LoginState.OnSuccessGetUserDetail -> {
                val status = state.userDetailModel.status

                if (status == "200"){
                    val data = state.userDetailModel.data

                    session.save(Constant.USERNAME, data.username)
                    session.save(Constant.EMAIL, data.emails[0].address)
                    session.save(Constant.EMAIL_VERIFIED, data.emails[0].verified)
                    session.save(Constant.NRP, data.profile.nrp)
                    session.save(Constant.PHONE, data.profile.phone)
                    session.save(Constant.JK, data.profile.jenis_kelamin)
                    session.save(Constant.KESATUAN, data.profile.kesatuan)

                    getDataSize()

                } else {
                    session.clear()
                    btn_signin.revertAnimation()

                    toast("Username or Password is wrong !!!")
                }
            }

            is LoginState.OnSuccessGetSize -> {
                val status = state.sizeModel.status

                if (status == "200"){
                    val data = state.sizeModel.data

                    for (i in data.indices){
                        getPerlengkapanById(data[i])
                    }

                    btn_signin.revertAnimation{
                        val intent = Intent(this@LoginActivity, MainActivity::class.java)
                        startActivity(intent)
                        this@LoginActivity.finish()
                    }
                }
            }

            is LoginState.OnErrorState -> {
                btn_signin.revertAnimation()
                toast("Login failed, check username & password or try again later!")
                logE(TAG, state.t.toString())
            }
        }
    }

    private fun addOutfit(data: OutfitEntity){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().addOutfit(data)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

            },{
                logE(TAG, it.toString())
            }))
    }

    private fun getPerlengkapanById(data: SizeModel.Data){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getPerlengkapanById(data.id)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                addOutfit(OutfitEntity(it._id, it.name, data.value, it.category, it.subCategory, session[Constant.USER_ID]!!))
            },{
                logE(TAG, it.toString())
            }))
    }

    override fun onDestroy() {
        super.onDestroy()
        btn_signin.dispose()
    }
}