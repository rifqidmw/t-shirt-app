package com.rifqidmw.tshirtapp.ui.register

import com.rifqidmw.tshirtapp.data.local.model.KesatuanModel
import com.rifqidmw.tshirtapp.data.local.model.RegisterModel

sealed class RegisterState {
    data class OnSuccessRegister(val registerModel: RegisterModel) : RegisterState()
    data class OnSuccessGetKesatuan(val kesatuanModel: KesatuanModel) : RegisterState()
    data class OnErrorState(val t: Throwable) : RegisterState()
}