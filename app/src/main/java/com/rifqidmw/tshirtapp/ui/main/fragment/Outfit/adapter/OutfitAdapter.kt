package com.rifqidmw.tshirtapp.ui.main.fragment.Outfit.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rifqidmw.tshirtapp.R
import com.rifqidmw.tshirtapp.data.local.MainDatabase
import com.rifqidmw.tshirtapp.data.local.entity.KesatuanEntity
import com.rifqidmw.tshirtapp.data.local.entity.OutfitEntity
import com.rifqidmw.tshirtapp.data.local.model.SizeModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject
import org.koin.java.KoinJavaComponent.inject
import java.util.*
import kotlin.collections.ArrayList

class OutfitAdapter(private val mContext: Context)
    : RecyclerView.Adapter<OutfitAdapter.MyViewHolder>() {

    private val TAG = OutfitAdapter::class.java.simpleName
    private val compositeDisposable = CompositeDisposable()
    private var mainDatabase: MainDatabase = MainDatabase.getInstance(mContext)!!
    private var dataList: MutableList<OutfitEntity> = ArrayList()

    private lateinit var itemView: View

    var listener: OutfitAdapterClickListener? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): OutfitAdapter.MyViewHolder {
        itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_outfit, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: OutfitAdapter.MyViewHolder, position: Int) {
        val data = dataList[position]

        holder.tvName.text = data.pakaian
        holder.tvSize.text = "Size/Type: ${data.value}"

        val category = if (data.subKategori.isEmpty()){
            data.kategori
        } else {
            "${data.subKategori}, ${data.kategori}"
        }

        holder.tvCategory.text = category

        getImage(data._id, holder.imgCover)

        holder.itemView.setOnLongClickListener {
            listener?.onClickListener(data)
            true
        }
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var tvName: TextView = itemView.findViewById(R.id.tv_title)
        internal var tvSize: TextView = itemView.findViewById(R.id.tv_desc)
        internal var tvCategory: TextView = itemView.findViewById(R.id.tv_category)
        internal var imgCover: ImageView = itemView.findViewById(R.id.img_cover)

    }

    fun setFilter(newList: MutableList<OutfitEntity>) {
        dataList = ArrayList<OutfitEntity>()
        dataList.addAll(newList)
        notifyDataSetChanged()
    }

    fun updateData(data: MutableList<OutfitEntity>) {
        val diffResult = DiffUtil.calculateDiff(
            OutfitAdapterDiffUtilCallback(this.dataList, data)
        )
        this.dataList.clear()
        this.dataList.addAll(data)
        diffResult.dispatchUpdatesTo(this)
    }

    private fun getImage(id: String, imgCover: ImageView){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getPerlengkapanById(id)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Glide.with(mContext).load(it.imgurl).into(imgCover)
            },{}))
    }
}