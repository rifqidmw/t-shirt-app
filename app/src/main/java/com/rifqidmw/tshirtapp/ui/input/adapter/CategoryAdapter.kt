package com.rifqidmw.tshirtapp.ui.input.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rifqidmw.tshirtapp.R
import com.rifqidmw.tshirtapp.data.local.entity.KategoriEntity
import com.rifqidmw.tshirtapp.data.local.entity.PerlengkapanEntity

class CategoryAdapter(private val mContext: Context, private var dataList: MutableList<KategoriEntity>, private var type: String)
    : RecyclerView.Adapter<CategoryAdapter.MyViewHolder>() {

    private val TAG = CategoryAdapter::class.java.simpleName
    private lateinit var itemView: View

    var listener: CategoryAdapterClickListener? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CategoryAdapter.MyViewHolder {
        itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_kesatuan, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CategoryAdapter.MyViewHolder, position: Int) {
        val data = dataList[position]

        holder.tvName.text = data.name

        holder.itemView.setOnClickListener {
            listener?.onItemCategoryClicked(data, type)
        }
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var tvName: TextView = itemView.findViewById(R.id.tv_name)
    }

    fun updateData(data: ArrayList<KategoriEntity>){
        this.dataList.clear()
        this.dataList.addAll(data)

        notifyDataSetChanged()
    }

    fun setFilter(newList: MutableList<KategoriEntity>) {
        dataList = java.util.ArrayList<KategoriEntity>()
        dataList.addAll(newList)
        notifyDataSetChanged()
    }

}