package com.rifqidmw.tshirtapp.ui.editProfile.adapter

import android.view.View
import com.rifqidmw.tshirtapp.data.local.entity.KesatuanEntity
import com.rifqidmw.tshirtapp.data.local.model.KesatuanModel

interface RecyclerViewClickListener {

    fun onItemKesatuanClicked(data: KesatuanEntity)

}