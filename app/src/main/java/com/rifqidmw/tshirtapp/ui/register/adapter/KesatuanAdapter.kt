package com.rifqidmw.tshirtapp.ui.register.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.rifqidmw.tshirtapp.R
import com.rifqidmw.tshirtapp.data.local.entity.KesatuanEntity
import com.rifqidmw.tshirtapp.data.local.entity.PerlengkapanEntity
import com.rifqidmw.tshirtapp.data.local.model.KesatuanModel
import com.rifqidmw.tshirtapp.data.local.model.NewsModel

class KesatuanAdapter(private val mContext: Context, private var dataList: MutableList<KesatuanEntity>)
    : RecyclerView.Adapter<KesatuanAdapter.MyViewHolder>() {

    private val TAG = KesatuanAdapter::class.java.simpleName
    private lateinit var itemView: View

    var listener: RecyclerViewClickListener? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): KesatuanAdapter.MyViewHolder {
        itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_kesatuan, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: KesatuanAdapter.MyViewHolder, position: Int) {
        val data = dataList[position]

        holder.tvName.text = data.name

        holder.itemView.setOnClickListener {
            listener?.onItemKesatuanClicked(data)
        }
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var tvName: TextView = itemView.findViewById(R.id.tv_name)
    }

    fun updateData(data: ArrayList<KesatuanEntity>){
        this.dataList.clear()
        this.dataList.addAll(data)

        notifyDataSetChanged()
    }

    fun setFilter(newList: MutableList<KesatuanEntity>) {
        dataList = java.util.ArrayList<KesatuanEntity>()
        dataList.addAll(newList)
        notifyDataSetChanged()
    }
}