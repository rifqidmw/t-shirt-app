package com.rifqidmw.tshirtapp.ui.editProfile

import com.google.gson.JsonObject
import com.rifqidmw.novel_kun.utls.RxViewModel
import com.rifqidmw.tshirtapp.data.local.model.LoginModel
import com.rifqidmw.tshirtapp.data.local.model.UserDetailModel
import com.rifqidmw.tshirtapp.data.repo.UserRepo
import com.rifqidmw.tshirtapp.utils.logD
import com.rifqidmw.tshirtapp.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class EditProfileViewModel (val userRepo: UserRepo): RxViewModel<EditProfileState>(), IEditProfileViewModel {
    override val TAG: String = EditProfileState::class.java.simpleName
    override fun successEditProfile(userDetailModel: UserDetailModel) {
        logD(TAG,"success update profile")
        state.value = EditProfileState.OnSuccessUpdateProfile(userDetailModel)
    }

    override fun successLogin(loginModel: LoginModel) {
        logD(TAG,"success login")
        state.value = EditProfileState.OnSuccessLogin(loginModel)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = EditProfileState.OnErrorState(t)
    }

    fun doUpdateData(header:Map<String, String>, data: JsonObject, userId: String) {
        logD(TAG,"do update user")
        launch {

            userRepo.doUpdateProfile(header, data, userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successEditProfile) {
                    errorLoad(t = it)
                }
        }
    }

    fun doLogin(header:Map<String, String>,data: JsonObject) {
        logD(TAG,"do login start")
        launch {

            userRepo.doLogin(header, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successLogin) {
                    errorLoad(t = it)
                }
        }
    }
}