package com.rifqidmw.tshirtapp.ui.main.fragment.Outfit

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import berhane.biniam.swipy.swipe.whenSwipedTo
import br.com.simplepass.loadingbutton.customViews.CircularProgressButton
import com.google.gson.JsonObject
import com.rifqidmw.tshirtapp.R
import com.rifqidmw.tshirtapp.data.local.MainDatabase
import com.rifqidmw.tshirtapp.data.local.Session
import com.rifqidmw.tshirtapp.data.local.entity.OutfitEntity
import com.rifqidmw.tshirtapp.data.local.entity.PerlengkapanEntity
import com.rifqidmw.tshirtapp.data.local.model.SizeModel
import com.rifqidmw.tshirtapp.ui.main.fragment.Outfit.adapter.OutfitAdapter
import com.rifqidmw.tshirtapp.ui.main.fragment.Outfit.adapter.OutfitAdapterClickListener
import com.rifqidmw.tshirtapp.ui.main.fragment.Outfit.adapter.RadioSizeAdapter
import com.rifqidmw.tshirtapp.ui.main.fragment.Outfit.adapter.RadioSizeAdapterClickListener
import com.rifqidmw.tshirtapp.utils.Constant
import com.rifqidmw.tshirtapp.utils.logD
import com.rifqidmw.tshirtapp.utils.logE
import com.rifqidmw.tshirtapp.utils.toast
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.dialog_edit_size.*
import kotlinx.android.synthetic.main.fragment_outfit.*
import org.json.JSONException
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.List
import kotlin.collections.MutableList
import kotlin.collections.joinToString
import kotlin.collections.set

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ItemFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class OutfitFragment : Fragment(), Observer<OutfitState>, RadioSizeAdapterClickListener, OutfitAdapterClickListener {
    private val TAG: String = OutfitFragment::class.java.simpleName

    private val session: Session by inject()
    private val mViewModel : OutfitViewModel by viewModel()
    private val compositeDisposable = CompositeDisposable()
    private val mainDatabase: MainDatabase by inject()

    private var param1: String? = null
    private var param2: String? = null
    private var valueOutfit: OutfitEntity? = null
    private var valueSize:Int = 0
    private var dataItem: ArrayList<OutfitEntity> = ArrayList()
    private var dataSize: ArrayList<String> = ArrayList()

    private lateinit var outfitAdapter: OutfitAdapter
    private lateinit var radioSizeAdapter: RadioSizeAdapter
    private lateinit var alertDialog: AlertDialog
    private lateinit var progressBar: ProgressBar
    private lateinit var btnUpdate: CircularProgressButton
    private lateinit var searchView: SearchView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            /*param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)*/
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_outfit, container, false)

        mViewModel.state.observe(viewLifecycleOwner,this)

        setupView(view)
        getOutfit()

        return view
    }
    @SuppressLint("UseCompatLoadingForDrawables")
    fun setupView(view: View){
        progressBar = view.findViewById(R.id.progress_bar) as ProgressBar

        searchView = view.findViewById(R.id.search_view) as SearchView
        val rvItem = view.findViewById(R.id.rv_item) as RecyclerView
        val linearLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        outfitAdapter = OutfitAdapter(requireContext())
        outfitAdapter.listener = this

        rvItem.layoutManager = linearLayoutManager

        rvItem.itemAnimator = DefaultItemAnimator()!!
        rvItem.adapter = outfitAdapter

        /*rvItem.whenSwipedTo(requireContext()){
            right {
                color = Color.TRANSPARENT
                icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_delete_box_red)
                callback {
                    outfitAdapter.notifyDataSetChanged()
                }
            }
            longRight {
                color = Color.TRANSPARENT
                icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_delete_box_red)
                callback {
                    outfitAdapter.notifyDataSetChanged()
                }
            }

            left {
                color = Color.TRANSPARENT
                icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_edit_box_yellow)
                callback {
                    outfitAdapter.notifyDataSetChanged()
                }
            }

            longLeft {
                color = Color.TRANSPARENT
                icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_edit_box_yellow)
                callback {
                    outfitAdapter.notifyDataSetChanged()
                }
            }
        }*/

        searchView.imeOptions = EditorInfo.IME_ACTION_SEARCH

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var textSearch = newText.toString()
                textSearch = textSearch.toLowerCase()
                var newList: MutableList<OutfitEntity> = ArrayList()
                if (textSearch.isEmpty()) {
                    newList = dataItem.toMutableList()
                } else {
                    for (model in dataItem) {
                        val title: String = model.pakaian.toLowerCase()
                        if (title.contains(textSearch)) {
                            newList.add(model)
                        }
                    }
                }
                outfitAdapter.setFilter(newList)

                return false
            }
        })
    }

    private fun showDialogCategory(data: PerlengkapanEntity, data1: OutfitEntity){
        dataSize.clear()
        dataSize.addAll(data.ukuran)

        logD("ukuran", data.ukuran.joinToString())
        val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.dialog_edit_size, null)

        val mBuilder = AlertDialog.Builder(requireContext())
            .setView(mDialogView)

        alertDialog = mBuilder.create()
        alertDialog.window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )

        alertDialog.window!!.setDimAmount(0f)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));

        alertDialog = mBuilder.show()

        alertDialog.tv_title.text = data.name

        btnUpdate = alertDialog.findViewById<CircularProgressButton>(R.id.btn_update)!!
        val rvSize = alertDialog.findViewById<RecyclerView>(R.id.rv_size)
        val linearLayoutManager = GridLayoutManager(requireContext(), 4)
        radioSizeAdapter = RadioSizeAdapter(requireContext(), dataSize)

        rvSize!!.layoutManager = linearLayoutManager

        rvSize.itemAnimator = DefaultItemAnimator()
        rvSize.adapter = radioSizeAdapter

        radioSizeAdapter.listener = this

        radioSizeAdapter.updateData(data1.value)

        alertDialog.btn_close.setOnClickListener {
            alertDialog.dismiss()
            resetData()
        }

        alertDialog.btn_cancel.setOnClickListener {
            alertDialog.dismiss()
            resetData()
        }

        alertDialog.btn_update.setOnClickListener {
            btnUpdate.startAnimation()
            doInputSize()
        }
    }

    private fun resetData(){
        valueOutfit = null
        valueSize = 0

        radioSizeAdapter.resetAdapter()
    }

    private fun getOutfit(){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getOutfit(session[Constant.USER_ID]!!)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                progressBar.visibility = View.GONE

                dataItem.clear()
                dataItem.addAll(it)
                outfitAdapter.updateData(it.toMutableList())

                if (dataItem.size > 0){
                    layout_data_empty.visibility = View.GONE
                } else {
                    layout_data_empty.visibility = View.VISIBLE
                }
            },{}))
    }
    
    private fun updateOutfit(){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().updateOutfitValue(session[Constant.USER_ID]!!, valueOutfit!!._id, dataSize[valueSize-1])}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                logD(TAG, "update id: ${valueOutfit!!._id}, name: ${valueOutfit!!.pakaian}, value: ${dataSize[valueSize-1]}")
                btnUpdate.revertAnimation {
                    radioSizeAdapter.resetAdapter()
                    searchView.setQuery("", false)
                    alertDialog.dismiss()
                    resetData()
                    getOutfit()
                }
            },{}))
    }

    private fun getSizeById(data: OutfitEntity){
        compositeDisposable.add(Observable.fromCallable{mainDatabase.mainDao().getPerlengkapanById(data._id)}
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showDialogCategory(it, data)
            },{
                logE(TAG, it.toString())
            }))
    }

    private fun doInputSize(){
        if (valueOutfit != null && valueSize != 0){
            btnUpdate.startAnimation()

            val headers = HashMap<String, String>()
            headers["Content-Type"] = "application/json"
            headers["X-Auth-Token"] = session[Constant.AUTH_TOKEN].toString()
            headers["X-User-Id"] = session[Constant.USER_ID].toString()

            val jsonObject = JsonObject()
            try {
                jsonObject.addProperty(valueOutfit!!._id, dataSize[valueSize-1])
                mViewModel.doAddSize(headers, jsonObject)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        } else {
            requireContext().toast("Please choose both of category and size!!")
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            OutfitFragment().apply {
                arguments = Bundle().apply {
                    /*putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)*/
                }
            }
    }

    override fun onChanged(state: OutfitState?) {
        when(state){
            is OutfitState.OnSuccessAddSize -> {
                val status = state.sizeModel.status

                if (status == "200"){
                    val data = state.sizeModel.data
                    onSuccessInputSize(data)
                }
            }

            is OutfitState.OnErrorState -> {
                progressBar.visibility = View.GONE
                requireContext().toast("Failed load data, please try again later or re login!!")
            }
        }
    }

    private fun onSuccessGetSize(data: List<OutfitEntity>){
        progressBar.visibility = View.GONE
        dataItem.clear()
        dataItem.addAll(data)
        outfitAdapter.notifyDataSetChanged()

        if (dataItem.size > 0){
            layout_data_empty.visibility = View.GONE
        } else {
            layout_data_empty.visibility = View.VISIBLE
        }
    }

    private fun onSuccessInputSize(data: List<SizeModel.Data>){
        updateOutfit()
    }

    override fun onItemSizeClicked(position: Int) {
        valueSize = position + 1
    }

    override fun onResume() {
        super.onResume()
        getOutfit()
    }

    override fun onClickListener(data: OutfitEntity) {
        valueOutfit = data
        getSizeById(data)
    }
}