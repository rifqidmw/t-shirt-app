package com.rifqidmw.tshirtapp.ui.splash

import com.rifqidmw.tshirtapp.data.local.model.KategoriModel
import com.rifqidmw.tshirtapp.data.local.model.KesatuanModel
import com.rifqidmw.tshirtapp.data.local.model.PerlengkapanModel

sealed class SplashState {
    data class OnSuccessGetKesatuan(val kesatuanModel: KesatuanModel) : SplashState()
    data class OnSuccessGetPerlengkapan(val perlengkapanModel: PerlengkapanModel) : SplashState()
    data class OnSuccessGetKategori(val kategoriModel: KategoriModel) : SplashState()
    data class OnErrorState(val t: Throwable) : SplashState()
}