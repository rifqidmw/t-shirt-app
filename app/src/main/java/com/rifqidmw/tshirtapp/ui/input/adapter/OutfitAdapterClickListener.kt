package com.rifqidmw.tshirtapp.ui.input.adapter

import android.view.View
import com.rifqidmw.tshirtapp.data.local.entity.PerlengkapanEntity
import com.rifqidmw.tshirtapp.data.local.model.KesatuanModel

interface OutfitAdapterClickListener {

    fun onItemOutfitClicked(data: PerlengkapanEntity)

}