package com.rifqidmw.tshirtapp.ui.main.fragment.Outfit.adapter

import com.rifqidmw.tshirtapp.data.local.entity.OutfitEntity

interface OutfitAdapterClickListener {
    fun onClickListener(data: OutfitEntity)
}