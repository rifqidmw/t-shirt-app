package com.rifqidmw.tshirtapp.ui.main.fragment.news

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rifqidmw.tshirtapp.R
import com.rifqidmw.tshirtapp.data.local.Session
import com.rifqidmw.tshirtapp.data.local.model.NewsModel
import com.rifqidmw.tshirtapp.ui.detailNews.DetailNewsActivity
import com.rifqidmw.tshirtapp.ui.input.adapter.RadioSizeAdapter
import com.rifqidmw.tshirtapp.ui.main.fragment.news.adapter.NewsAdapter
import com.rifqidmw.tshirtapp.utils.Constant
import com.rifqidmw.tshirtapp.utils.toast
import com.utsman.recycling.extentions.Recycling
import com.utsman.recycling.setupAdapter
import kotlinx.android.synthetic.main.activity_input.*
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.android.synthetic.main.item_news.view.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.HashMap

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class NewsFragment : Fragment(), Observer<NewsState> {
    private val session: Session by inject()
    private val mViewModel : NewsViewModel by viewModel()

    private var param1: String? = null
    private var param2: String? = null
    private var dataNews: ArrayList<NewsModel.Data> = ArrayList()

    private lateinit var newsAdapter: NewsAdapter
    private lateinit var progressBar: ProgressBar
    private lateinit var recyclingNews: Recycling<NewsModel.Data>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            /*param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)*/
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_news, container, false)

        mViewModel.state.observe(viewLifecycleOwner,this)

        setupView(view)

        return view
    }

    private fun setupView(view: View){
        progressBar = view.findViewById(R.id.progress_bar) as ProgressBar

        val rvNews = view.findViewById(R.id.rv_news) as RecyclerView

        rvNews.setupAdapter<NewsModel.Data>(R.layout.item_news){ adapter, context, list ->

            recyclingNews = this@setupAdapter

            bind { itemView, position, item ->
                itemView.tv_title.text = item?.title
                itemView.tv_desc.text = item?.content
                itemView.tv_date.text = item?.createdAt

                itemView.setOnClickListener {
                    val intent = Intent(requireContext(), DetailNewsActivity::class.java)
                    intent.putExtra("NEWS_TITLE", item?.title)
                    intent.putExtra("NEWS_CONTENT", item?.content)
                    intent.putExtra("NEWS_DATE", item?.createdAt)
                    startActivity(intent)
                }
            }

            addLoader(R.layout.layout_loader){
                idLoader = R.id.progress_circular
                idTextError = R.id.error_text_view
            }

            val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            setLayoutManager(layoutManager)

            getDataNews()

            onPagingListener(layoutManager){page, itemCount ->
                if (dataNews.size > 9){
//                    getDataNews()
                }
            }
        }
    }

    private fun getDataNews(){
        progressBar.visibility = View.VISIBLE

        val headers = HashMap<String, String>()
        headers["Content-Type"] = "application/json"
        headers["X-Auth-Token"] = session[Constant.AUTH_TOKEN].toString()
        headers["X-User-Id"] = session[Constant.USER_ID].toString()

        mViewModel.doGetNews(headers)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            NewsFragment().apply {
                arguments = Bundle().apply {
                    /*putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)*/
                }
            }
    }

    private fun onSuccessGetNews(data: ArrayList<NewsModel.Data>){
        dataNews.addAll(data)
        recyclingNews.submitList(data)

        if (dataNews.size > 0){
            layout_data_empty.visibility = View.GONE
        } else {
            layout_data_empty.visibility = View.VISIBLE
        }
    }

    override fun onChanged(state: NewsState?) {
        when(state){
            is NewsState.OnSuccessGetNews -> {
                progressBar.visibility = View.GONE

                val status = state.newsModel.status
                val msg = state.newsModel.msg
                if (status == "200"){

                    val data = state.newsModel.data
                    onSuccessGetNews(data)

                } else {
                    requireActivity().toast(msg)
                }
            }

            is NewsState.OnErrorState -> {
                progressBar.visibility = View.GONE

                requireActivity().toast("Failed load news, try again later")
            }
        }
    }
}