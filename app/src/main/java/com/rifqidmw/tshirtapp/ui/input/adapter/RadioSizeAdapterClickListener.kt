package com.rifqidmw.tshirtapp.ui.input.adapter

import android.view.View
import com.rifqidmw.tshirtapp.data.local.model.KesatuanModel

interface RadioSizeAdapterClickListener {

    fun onItemSizeClicked(position: Int)

}