package com.rifqidmw.tshirtapp.ui.main.fragment.Outfit

import com.google.gson.JsonObject
import com.rifqidmw.novel_kun.utls.RxViewModel
import com.rifqidmw.tshirtapp.data.local.model.SizeModel
import com.rifqidmw.tshirtapp.data.repo.OutfitRepo
import com.rifqidmw.tshirtapp.ui.main.fragment.news.NewsState
import com.rifqidmw.tshirtapp.utils.logD
import com.rifqidmw.tshirtapp.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class OutfitViewModel (val outfitRepo: OutfitRepo): RxViewModel<OutfitState>(), IOutfitViewModel {
    override val TAG: String = OutfitState::class.java.simpleName

    override fun successGetSize(sizeModel: SizeModel) {
        logD(TAG,"success get size")
        state.value = OutfitState.OnSuccessGetSize(sizeModel)
    }

    override fun successAddSize(sizeModel: SizeModel) {
        logD(TAG,"success add size")
        state.value = OutfitState.OnSuccessAddSize(sizeModel)
    }

    override fun errorLoad(t: Throwable) {
        logE(TAG,"error load : ${t.message}")
        state.value = OutfitState.OnErrorState(t)
    }

    fun doGetSize(header: Map<String, String>) {
        logD(TAG,"do load size")
        launch {
            outfitRepo.getSize(header)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successGetSize,this::errorLoad)
        }
    }

    fun doAddSize(header:Map<String, String>,data: JsonObject) {
        logD(TAG,"do add size: $data")
        launch {
            outfitRepo.doAddSize(header, data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::successAddSize) {
                    errorLoad(t = it)
                }
        }
    }
}