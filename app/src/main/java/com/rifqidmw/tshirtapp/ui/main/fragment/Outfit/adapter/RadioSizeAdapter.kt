package com.rifqidmw.tshirtapp.ui.main.fragment.Outfit.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.rifqidmw.tshirtapp.R

class RadioSizeAdapter(private val mContext: Context, private var dataList: MutableList<String>)
    : RecyclerView.Adapter<RadioSizeAdapter.MyViewHolder>() {

    private val TAG = RadioSizeAdapter::class.java.simpleName
    private lateinit var itemView: View
    var mSelectedItem = -1
    var listener: RadioSizeAdapterClickListener? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RadioSizeAdapter.MyViewHolder {
        itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_size, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RadioSizeAdapter.MyViewHolder, position: Int) {
        val data = dataList[position]

        if (position == mSelectedItem){
            holder.radioButton.typeface = ResourcesCompat.getFont(mContext, R.font.montserrat_bold)
        } else {
            holder.radioButton.typeface = ResourcesCompat.getFont(mContext, R.font.montserrat)
        }

        holder.radioButton.isChecked = position == mSelectedItem

        holder.radioButton.text = data
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var radioButton: RadioButton = itemView.findViewById(R.id.radio_button)

        init {
            val clickListener =
                View.OnClickListener {
                    mSelectedItem = adapterPosition
                    listener?.onItemSizeClicked(adapterPosition)
                    notifyDataSetChanged()
                }
            itemView.setOnClickListener(clickListener)
            radioButton.setOnClickListener(clickListener)
        }
    }

    fun updateData(selected: String){
        if (dataList.contains(selected)){
            for (i in dataList.indices){
                if (dataList[i] == selected){
                    mSelectedItem = i
                    listener?.onItemSizeClicked(i)
                    notifyDataSetChanged()
                }
            }
        }
    }

    fun resetAdapter(){
        mSelectedItem = -1
        notifyDataSetChanged()
    }
}