package com.rifqidmw.tshirtapp.ui.main.fragment.news

import com.rifqidmw.tshirtapp.data.local.model.NewsModel

interface INewsViewModel {
    fun successGetNews(newsModel: NewsModel)
    fun errorLoad(t: Throwable)
}