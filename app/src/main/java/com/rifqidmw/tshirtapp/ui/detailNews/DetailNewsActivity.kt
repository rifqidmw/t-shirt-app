package com.rifqidmw.tshirtapp.ui.detailNews

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.rifqidmw.tshirtapp.R
import kotlinx.android.synthetic.main.activity_detail_news.*

class DetailNewsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_news)

        setupView()
        setupIntent()
    }

    private fun setupView(){
        toolbar.setNavigationOnClickListener {
            onBackPressed()
            this@DetailNewsActivity.finish()
        }
    }

    private fun setupIntent(){
        intent.let {
            collapse_toolbar.title = it.getStringExtra("NEWS_TITLE")
            tv_content.text = it.getStringExtra("NEWS_CONTENT")
            tv_date.text = it.getStringExtra("NEWS_DATE")
        }
    }
}