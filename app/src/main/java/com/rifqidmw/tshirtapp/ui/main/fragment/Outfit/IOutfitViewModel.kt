package com.rifqidmw.tshirtapp.ui.main.fragment.Outfit

import com.rifqidmw.tshirtapp.data.local.model.SizeModel

interface IOutfitViewModel {
    fun successGetSize(sizeModel: SizeModel)
    fun successAddSize(sizeModel: SizeModel)
    fun errorLoad(t: Throwable)
}