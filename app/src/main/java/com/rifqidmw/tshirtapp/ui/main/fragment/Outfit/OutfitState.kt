package com.rifqidmw.tshirtapp.ui.main.fragment.Outfit

import com.rifqidmw.tshirtapp.data.local.model.SizeModel

sealed class OutfitState {
    data class OnSuccessGetSize(val sizeModel: SizeModel): OutfitState()
    data class OnSuccessAddSize(val sizeModel: SizeModel): OutfitState()
    data class OnErrorState(val t: Throwable): OutfitState()
}